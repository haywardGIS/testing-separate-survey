////////Get the data from the view, pass that data to the model that does some computation.
                //The returned data frrom the model is passed to the View for presentation.
var Controller = function (viewHelper, model) {
    var myModel = model;
    var myView = viewHelper;

    //store the number of water configs so other functions that don't have access to result.get("WaterConfigNumber") can get it
    var controllerWaterConfigNumber;

    //store whether or not to charge the inclusionary housing fee
    var inclusionaryHousingYesOrNo;

    //store whether or not there will be water fields. To be used in Home Repair to not get water values of perform water calculations
    var newWaterFeatures;

    //eventhandler for the button and the textfield
    function actionPerformed(load) {

        console.log("Recalculate");
        console.log("type of school district fee = " + typeof (myModel.calcNewDevelopmentResidentialSchoolDistrictFee(sfrHabitable, townhomeHabitable, condoHabitable, aptHabitable, sfrNonHabitable, townhomeNonHabitable, condoNonHabitable, aptNonHabitable).toFixed(2)));

        //if Home Repair, recalculate water
        if (myView.getTitle() === "Home Repair - Fee Estimate") {
            // Hide per unit fees since we aren't working with units in Home Repair
            myView.hidePerUnitFees();

            //if 1st loading of a project don't recalculate valuation, use what was paseed in to preserve a user entered value if it exists
            //if values are edited on estimator, then valuation will be recalculated based on all values in estimator
            if (load != "1st") {
                var valuation = myModel.getValuation("VB", "Residential, one- and two-family", myView.getNewHabitableSquareFootage()) +
                (myModel.getValuation("VB", "Residential, one- and two-family", myView.getExistingHabitableSquareFootage() * 0.6) +
                myModel.getValuation("VB", "Utility, miscellaneous", myView.getNewNonHabitableSquareFootage()) +
                (myModel.getValuation("VB", "Utility, miscellaneous", myView.getExistingNonHabitableSquareFootage()) * 0.6));
                myView.setValuation(valuation);
                console.log("valuation home repair, not 1st load" + valuation);
            }

            //get all sewer fees for residential, pass in existing meter and new meter requirement. might be the same.

            //Figure out the new water fees and water meter size
            if (myView.getNewWaterFixturesYN() === "Yes") {
                var newWaterMeter = myModel.figureResidentialWaterMeter(parseInt(myView.getShowerTubCombo()),
                                                                    parseInt(myView.getTubOnly()),
                                                                    parseInt(myView.getShowerOnly()),
                                                                    parseInt(myView.getJacuzzi()),
                                                                    parseInt(myView.getBathSink()),
                                                                    parseInt(myView.getToilet()),
                                                                    parseInt(myView.getKitchenSink()),
                                                                    parseInt(myView.getDishwasher()),
                                                                    parseInt(myView.getLaundrySink()),
                                                                    parseInt(myView.getClothesWasher()),
                                                                    parseInt(myView.getGarageSink()),
                                                                    parseInt(myView.getBarSink()),
                                                                    parseInt(myView.getHoseBib()));
                myView.setNewWaterMeter(newWaterMeter);
                //Use water meters to determine water fee
                var waterFees = myModel.calcHomeRepairWaterFees(myView.getCurrentWaterMeter(), newWaterMeter);
                console.log(waterFees);
                myView.setWaterMeterFee(numberWithCommas(waterFees[0].toFixed(2)));
                myView.setRemoteFee(numberWithCommas(waterFees[1].toFixed(2)));
                myView.setSewerConnectionFee((0).toFixed(2));
            }
            // else don't do water calculations
            else {
                myView.setWaterMeterFee((0).toFixed(2));
                myView.setRemoteFee((0).toFixed(2));
                myView.setSewerConnectionFee((0).toFixed(2));
            }

            //get new square footage and pass that as param for calcSchoolDistrictFee to derive school district fees
            console.log("school fee in controller = " + myModel.calcHomeRepairSchoolDistrictFee(myView.getNewHabitableSquareFootage()).toFixed(2));
            myView.setSchoolDistrictFee(numberWithCommas(myModel.calcHomeRepairSchoolDistrictFee(myView.getNewHabitableSquareFootage()).toFixed(2)));
            myView.setParkDedicationFee((0).toFixed(2));
            myView.setConstructionTax((0).toFixed(2));
            myView.setSupplementalConstructionTax((0).toFixed(2));
            myView.setInclusionaryHousingFee((0).toFixed(2));
        }

        //IF NEW DEVELOPMENT RESIDENTIAL
        if (myView.getTitle() === "New Residential - Fee Estimate") {
            // Show per unit fees if they were hidden for Home Repair previously
            if ($('.fees-per-unit').hasClass('hide')) {
                myView.showPerUnitFees();
            }

            //if 1st loading of a project don't recalculate valuation, use what was paseed in to preserve a user entered value if it exists
            //if values are edited on estimator, then valuation will be recalculated based on all values in estimator
            if (load != "1st") {
                //get square footage and development building type and calculate new valuation
                if ($(".section-title").first().text() === "Single Family Homes") {
                    console.log(myView.getSFRHabitable());
                    console.log(myView.getSFRNonHabitable());
                    var valuation = myModel.getValuation("VB", "Residential, one- and two-family", myView.getSFRHabitable()) + myModel.getValuation("VB", "Utility, miscellaneous", myView.getSFRNonHabitable());
                    console.log(valuation);
                } else if ($(".section-title").first().text() === "Townhomes") {
                    //decide whether to use single family or multi family for townhome valuations
                    if (document.getElementById('townhome-type').value === "Individual Lots") {
                        var valuation = myModel.getValuation("VB", "Residential, one- and two-family", myView.getTownhomeHabitable()) + myModel.getValuation("VB", "Utility, miscellaneous", myView.getTownhomeNonHabitable());
                    } else if (document.getElementById('townhome-type').value === "Group Lots") {
                        var valuation = myModel.getValuation("VB", "Residential, multiple family", myView.getTownhomeHabitable()) + myModel.getValuation("VB", "Utility, miscellaneous", myView.getTownhomeNonHabitable());
                    }
                } else if ($(".section-title").first().text() === "Condos") {
                    var valuation = myModel.getValuation("VB", "Residential, multiple family", myView.getCondoHabitable()) + myModel.getValuation("VB", "Utility, miscellaneous", myView.getCondoNonHabitable());
                } else if ($(".section-title").first().text() === "Apartments") {
                    var valuation = myModel.getValuation("VB", "Residential, multiple family", myView.getAptHabitable()) + myModel.getValuation("VB", "Utility, miscellaneous", myView.getAptNonHabitable());
                }
                myView.setValuation(valuation);
            }

            //if New Development, recalculate water
            var waterMeterFees;
            var wcArrayFromDivs = [];


            //if units for each type exists, convert to number and assign to variable
            var sfrU1500SqFt, sfrLess1500SqFt, sfrHabitable, sfrNonHabitable, sfrGranny, sfrDuplex, sfrStandalone;
            var townhomeU, townhomeType, townhomeHabitable, townhomeNonHabitable, sfrTownhome1500SqFt, sfrTownhomeLess1500SqFt, multiTownhome800SqFt, multiTownhomeLess800SqFt;
            var condoU, condoSmallBldgs, condoNotSmallBldgs, condo800SqFt, condoLess800SqFt, condoHabitable, condoNonHabitable;
            var aptU, aptSmallBldgs, aptNotSmallBldgs, apt800SqFt, aptLess800SqFt, aptHabitable, aptNonHabitable;
            var totalU;
            //check to see if standalone/duplex/inlaws fields exist indicating there are sfr units; changed from project based calculator since we no
            //longer show total sfr unit, but a breakdown of standalone/duplex/granny
            if (!isNaN(myView.getSFRStandalone()) && !isNaN(myView.getSFRDuplexes()) && !isNaN(myView.getSFRInLaws())) {
                sfr1500SqFt = parseInt(myView.getSFR1500());
                sfrHabitable = parseInt(myView.getSFRHabitable());
                sfrNonHabitable = parseInt(myView.getSFRNonHabitable());
                sfrGranny = parseInt(myView.getSFRInLaws());
                sfrStandalone = parseInt(myView.getSFRStandalone());
                sfrDuplex = parseInt(myView.getSFRDuplexes());
                // add up all sfr units and subtract units > 1500 sq ft to get units < 1500 sq ft
                sfrLess1500SqFt = (sfrGranny + sfrStandalone + sfrDuplex) - sfr1500SqFt;
            } else {
                sfr1500SqFt = 0;
                sfrLess1500SqFt = 0;
                sfrHabitable = 0;
                sfrNonHabitable = 0;
                sfrGranny = 0;
                sfrStandalone = 0;
                sfrDuplex = 0;
            }
            //TOWNHOME UNITS
            if (!isNaN(myView.getTownhomeUnits())) {
                townhomeU = parseInt(myView.getTownhomeUnits());
                townhomeType = myView.getTownhomeType();
                townhomeHabitable = parseInt(myView.getTownhomeHabitable());
                townhomeNonHabitable = parseInt(myView.getTownhomeNonHabitable());
                //check which type of townhoome and assign values for sfr units (individual lots) of multi family units (group lot)
                if (townhomeType === "Individual Lots") {
                    //use townhomeSize since it is used for bot 800 sq ft and 1500 sq ft questions depending on the type of townhomes
                    sfrTownhome1500SqFt = parseInt(myView.getTownhomeSize());
                    sfrTownhomeLess1500SqFt = townhomeU - sfrTownhome1500SqFt;
                    multiTownhome800SqFt = 0;
                    multiTownhomeLess800SqFt = 0;
                }
                else {
                    sfrTownhome1500SqFt = 0;
                    sfrTownhomeLess1500SqFt = 0;
                    multiTownhome800SqFt = parseInt(myView.getTownhomeSize());
                    multiTownhomeLess800SqFt = townhomeU - multiTownhome800SqFt;
                }
            } else {
                townhomeU = 0;
                townhomeType = 0;
                townhomeHabitable = 0;
                townhomeNonHabitable = 0;
                sfrTownhome1500SqFt = 0;
                sfrTownhomeLess1500SqFt = 0;
                multiTownhome800SqFt = 0;
                multiTownhomeLess800SqFt = 0;
            }
            //CONDO UNITS
            if (!isNaN(myView.getCondoUnits())) {
                console.log("in condo if");
                condoU = parseInt(myView.getCondoUnits());
                if (condoU < 5) {
                    condoSmallBldgs = condoU;
                    condoNotSmallBldgs = 0;
                } else {
                    condoSmallBldgs = 0;
                    condoNotSmallBldgs = condoU;
                }
                condo800SqFt = parseInt(myView.getCondo800());
                condoLess800SqFt = condoU - condo800SqFt;
                condoHabitable = parseInt(myView.getCondoHabitable());
                condoNonHabitable = parseInt(myView.getCondoNonHabitable());
                console.log("small condo units = " + condoSmallBldgs);
                console.log("not small condo units = " + condoNotSmallBldgs);

            } else {
                condoU = 0;
                condoSmallBldgs = 0;
                condoNotSmallBldgs = 0;
                condo800SqFt = 0;
                condoLess800SqFt = 0;
                condoHabitable = 0;
                condoNonHabitable = 0;
            }
            //APT UNITS
            if (!isNaN(myView.getAptUnits())) {
                aptU = parseInt(myView.getAptUnits());
                if (aptU < 5) {
                    aptSmallBldgs = aptU;
                    aptNotSmallBldgs = 0;
                } else {
                    aptSmallBldgs = 0;
                    aptNotSmallBldgs = aptU;
                }
                apt800SqFt = parseInt(myView.getApt800());
                aptLess800SqFt = aptU - apt800SqFt;
                aptHabitable = parseInt(myView.getAptHabitable());
                aptNonHabitable = parseInt(myView.getAptNonHabitable());
            } else {
                aptU = 0;
                aptSmallBldgs = 0;
                aptNotSmallBldgs = 0;
                apt800SqFt = 0;
                aptLess800SqFt = 0;
                aptHabitable = 0;
                aptNonHabitable = 0;
            }

            totalU = sfrGranny + sfrStandalone + sfrDuplex + townhomeU + condoU + aptU;

            //array to help us dynamically update all water values, these are the endings of the div that we append to "wc" + number to get each individual div no matter how many configurations
            var arrayOfWaterDivEndings = ["-units", "-stcombo", "-tub-only", "-jacuzzi", "-shower-only", "-bath-sink", "-toilets", "-kitchen-sink", "-dishwasher", "-laundry-sink", "-clothes-washer",
         "-garage-sink", "-bar-sink", "-hose-bib"];
            console.log("waterConfigNumber in action = " + controllerWaterConfigNumber);

            for (i = 1; i <= controllerWaterConfigNumber; i++) {
                var currWaterConfigValues = [];
                //only 14 as we don't need to know the water meter that is there, we will figure the needed one from all the values
                for (j = 0; j < 14; j++) {
                    var thisWaterConfigDiv = "wc" + i + arrayOfWaterDivEndings[j];
                    console.log("thisWaterConfigDiv = " + thisWaterConfigDiv);
                    currWaterConfigValues.push(myView.getWaterConfigValues(thisWaterConfigDiv));
                }
                console.log("currWaterConfigValues = " + i + " " + currWaterConfigValues);
                //figure out what type of meter is needed
                var newWaterMeter = myModel.figureResidentialWaterMeter(parseInt(currWaterConfigValues[1]),
                                                                        parseInt(currWaterConfigValues[2]),
                                                                        parseInt(currWaterConfigValues[3]),
                                                                        parseInt(currWaterConfigValues[4]),
                                                                        parseInt(currWaterConfigValues[5]),
                                                                        parseInt(currWaterConfigValues[6]),
                                                                        parseInt(currWaterConfigValues[7]),
                                                                        parseInt(currWaterConfigValues[8]),
                                                                        parseInt(currWaterConfigValues[9]),
                                                                        parseInt(currWaterConfigValues[10]),
                                                                        parseInt(currWaterConfigValues[11]),
                                                                        parseInt(currWaterConfigValues[12]),
                                                                        parseInt(currWaterConfigValues[13]));
                console.log("new water meter = " + newWaterMeter);

                var thisMeterDiv = "wc" + i + "-watermeter";
                myView.setNewDevWaterMeter(thisMeterDiv, newWaterMeter);
                console.log()
                currWaterConfigValues.push(newWaterMeter);
                wcArrayFromDivs.push(currWaterConfigValues);
            }

            //CALCULATE WATER METER FEES
            console.log("wcArrayFromDivs total = " + wcArrayFromDivs);
            console.log("wcArrayFromDivs length = " + wcArrayFromDivs.length);
            waterMeterFees = myModel.calcNewDevelopmentWaterFees(wcArrayFromDivs);
            console.log(waterMeterFees);


            //CALCULATE SEWER CONNECTION FEES
            var sewerArray = [];
            //add up units for Single Family and Multi Family units for sewer connection fees
            //SFR for utilities = sfr, duplex, triplex, townhomes and all condo/apt in buildings with less than 5 units; also all planned developments (no matter the bldg type)
            console.log(townhomeU);
            console.log(condoU);
            console.log(aptU);
            //if Planned Development, all units are given the higher fee
            if (myView.getPlannedDevelopment() === "Yes") {
                sewerArray[0] = [sfrGranny + sfrStandalone + sfrDuplex + townhomeU + condoU + aptU];
                sewerArray[1] = [0];
                //if not a Planned Development, then sfr, all townhomes, all condos/apts in buildings with less than 5 units get the higher fee
            } else {
                console.log(sfrGranny);
                console.log(sfrStandalone);
                console.log(sfrDuplex);
                console.log(townhomeU);
                console.log(condoSmallBldgs);
                console.log(aptSmallBldgs);
                console.log(condoU);
                console.log(aptU);
                sewerArray[0] = [sfrGranny + sfrStandalone + sfrDuplex + townhomeU + condoSmallBldgs + aptSmallBldgs];
                sewerArray[1] = [(condoU - condoSmallBldgs) + (aptU - aptSmallBldgs)];
            }
            console.log(sewerArray);
            console.log("sewerFee = " + myModel.calcSewerConnectionFee(sewerArray));
            myView.setWaterMeterFee(numberWithCommas(waterMeterFees[0].toFixed(2)));
            myView.setRemoteFee(numberWithCommas(waterMeterFees[1].toFixed(2)));
            myView.setSewerConnectionFee(numberWithCommas(myModel.calcSewerConnectionFee(sewerArray).toFixed(2)));

            //CALCULATE CONSTRUCTION TAXES
            //Condos and Apts are always Multi-Family; Townhomes are single family if they are built on individual lots (var townhomeType), Duplexes, in-laws and standalone houses are all single family for this tax
            var constructionTaxArray = [];
            console.log("sfr1500 = " + sfr1500SqFt);
            console.log("townhome1500 = " + sfrTownhome1500SqFt);
            console.log("sfrLess1500 = " + sfrLess1500SqFt);
            console.log("sfrTownhomeLess1500 = " + sfrTownhomeLess1500SqFt)
            constructionTaxArray[0] = sfr1500SqFt + sfrTownhome1500SqFt;
            constructionTaxArray[1] = sfrLess1500SqFt + sfrTownhomeLess1500SqFt;
            constructionTaxArray[2] = apt800SqFt + condo800SqFt + multiTownhome800SqFt;
            constructionTaxArray[3] = aptLess800SqFt + condoLess800SqFt + multiTownhomeLess800SqFt;
            console.log("construction tax array = " + constructionTaxArray);
            myView.setConstructionTax(numberWithCommas(myModel.calcConstructionTax(constructionTaxArray).toFixed(2)));

            //CALCULATE SUPPLEMENTAL BUILDING CONSTRUCTION & IMPROVEMENT TAX = $1200 per unit
            myView.setSupplementalConstructionTax(numberWithCommas(myModel.calcSupplementalConstructionTax(totalU).toFixed(2)));

            //CALCULATE SCHOOL DISTRICT FEES
            // habitable sq ft X 2.97 + non habitable sq ft X 0.47
            //pass in all 8 habitable/non habitable values from the view
            myView.setSchoolDistrictFee(numberWithCommas(myModel.calcNewDevelopmentResidentialSchoolDistrictFee(sfrHabitable, townhomeHabitable, condoHabitable, aptHabitable, sfrNonHabitable, townhomeNonHabitable, condoNonHabitable, aptNonHabitable).toFixed(2)));

            //CALCULATE PARK DEDICATION FEES
            // All new homes are required to pay this one-time fee.

            // Single Family Detached
            // $11,953 per unit for each detached single family residence (no in-laws)

            // Single Family Attached
            // $11,395 per unit for each attached single family residence. Duplexes, townhomes (both indiviual lots
            // and group lots), condos

            // Multi-Family 
            // $9,653 per unit for any group of buildings or portion thereof that has 2 or more units for rent, 
            // including granny units (and the home the granny is in). Apt buildings and in-laws. An in-law differs
            // from a duplex in that a duplex has two owners, a granny/sfr combo has one.
            myView.setParkDedicationFee(numberWithCommas(myModel.calcParkDedicationFee(sfrDuplex, sfrStandalone, sfrGranny, townhomeU, condoU, aptU).toFixed(2)));

            //CALCULATE INCLUSIONARY HOUSING FEES
            // For projects > 20 units, this fee is either paid for each individual unit per sq ft or per project total habitable sq ft divided by units. Developers can decide.

            //Fees:
            // Single Family Detached (standlone homes) -		$4.00 sq habitable ft
            // Single Family Attached (duplex, condos, townhomes) -	$3.24 sq habitable ft
            // Multi Family Housing (apartments) -			$3.24 sq habitable ft

            // Payment timetable:
            // Fees as outlined above if paid upon issuance of building permits
            // Fees increase by 10% if paid with Certificate of Occupancy
            myView.setInclusionaryHousingFee(numberWithCommas(myModel.calcInclusionaryHousingFee(sfrDuplex, sfrStandalone, sfrGranny, townhomeU, condoU, aptU, inclusionaryHousingYesOrNo, sfrHabitable, townhomeHabitable, condoHabitable, aptHabitable).toFixed(2)));
        }
        ///END NEW DEVELOPMENT RESIDENTIAL CALCULATIONS

        //BUILDING FEES
        //get current valuation from view
        var valuation = parseInt(myView.getValuation());

        //send current value to model and calculate bldg inspection fee
        var insFee = parseFloat(myModel.calcInsFee(valuation));
        console.log("insFee i controller " + insFee);

        myView.setBldgInspFee(numberWithCommas(myModel.calcBldgInspectionFee(insFee).toFixed(2)));
        myView.setFireInspFee(numberWithCommas(myModel.calcFireInspectionFee().toFixed(2)));
        myView.setHazInspFee(numberWithCommas(myModel.calcHazmatInspectionFee().toFixed(2)));

        myView.setBldgPlanCheckFee(numberWithCommas(myModel.calcBuildingPlanCheckFee(insFee)));
        myView.setFireReviewFee(numberWithCommas(myModel.calcFireReviewFee(insFee)));
        myView.setPlanRevFee(numberWithCommas(myModel.calcPlanningReviewFee(insFee)));
        myView.setSolidWRevFee(numberWithCommas(myModel.calcSolidWasteReviewFee().toFixed(2)));

        myView.setTechFee(numberWithCommas(myModel.calcTechnologyFee(insFee)));
        myView.setCommPlanFee(numberWithCommas(myModel.calcCommunityPlanningFee(insFee)));
        myView.setPermIssFee(numberWithCommas(myModel.calcPermitIssuanceFee().toFixed(2)));
        myView.setSMIPFee(numberWithCommas(myModel.calcSMIPFee(valuation).toFixed(2)));
        myView.setBldgStandardsFee(numberWithCommas(myModel.calcBuildingStandardsFee(valuation).toFixed(2)));

        //CALCULATE TOTAL FEES BY COMBINING ALL FEES, PERTINENT TO ALL PROJECT TYPES
        // myView.setTotalFees(myModel.totalBuildingPermitFee(insFee));

        var totalFees = parseFloat(makeNumberAgain(myView.getBldgInspFee())) +
                        parseFloat(makeNumberAgain(myView.getFireInspFee())) +
                        parseFloat(makeNumberAgain(myView.getHazInspFee())) +
                        parseFloat(makeNumberAgain(myView.getBldgPlanCheckFee())) +
                        parseFloat(makeNumberAgain(myView.getFireReviewFee())) +
                        parseFloat(makeNumberAgain(myView.getPlanRevFee())) +
                        parseFloat(makeNumberAgain(myView.getSolidWRevFee())) +
                        parseFloat(makeNumberAgain(myView.getTechFee())) +
                        parseFloat(makeNumberAgain(myView.getCommPlanFee())) +
                        parseFloat(makeNumberAgain(myView.getPermIssFee())) +
                        parseFloat(makeNumberAgain(myView.getSMIPFee())) +
                        parseFloat(makeNumberAgain(myView.getBldgStandardsFee())) +
                        parseFloat(makeNumberAgain(myView.getWaterMeterFee())) +
                        parseFloat(makeNumberAgain(myView.getRemoteFee())) +
                        parseFloat(makeNumberAgain(myView.getSewerConnectionFee())) +
                        parseFloat(makeNumberAgain(myView.getSchoolDistrictFee())) +
                        parseFloat(makeNumberAgain(myView.getParkDedicationFee())) +
                        parseFloat(makeNumberAgain(myView.getInclusionaryHousingFee())) +
                        parseFloat(makeNumberAgain(myView.getConstructionTax())) +
                        parseFloat(makeNumberAgain(myView.getSupplementalConstructionTax()));
        console.log("Total Fees = " + totalFees);
        myView.setTotalFees(numberWithCommas(totalFees.toFixed(2)));

        console.log("uncomma a number = " + makeNumberAgain(myView.getParkDedicationFee()));

        //CALCULATE FEES PER UNIT
        var feesPerUnit = (totalFees / totalU);
        myView.setFeesPerUnit(numberWithCommas(feesPerUnit.toFixed(2)));
    }

    //remove commas from formatted number to use in calculations
    function makeNumberAgain(value) {
        return value.split(",").join('');
    }

    var token = "";

    function onloadPerformed(event) {
        // var token ="f8ff6081952a4150d23b79d6890963a6";

        var x = document.referrer;
        console.log(x);

        if (x === "") {
            console.log("in onLoadPerformed in if = " + x);

        } else {
            console.log("in onLoadPerformed in else = " + x);
            //get the data from the view and JSON
            var response = myModel.getOutput(token);
            var result = myModel.drawOutput(response["responseText"]);
            //make result available throughout the controller
            controllerResult = result;
            handleLoadResult(result);

        }


    }

    // on button click in modal on sample projects, run onSampleProjectButtonClick to load sample projects and close modal
    $(".btn-success").on('click', function (e) {

        onSampleProjectButtonClick(this.id);
    })

    // launch fee estimator from Modal window
    $("#new-estimate").on('click', function (e) {
        window.open("https://cityofhayward.typeform.com/to/mcffiv?", "_top");
    })

    // load old estimate from Modal window
    // if no token present, load error
    // if token present, whether valid or not, call ButtonClick
    $("#old-estimate").on('click', function (e) {
        // token = myView.getOldEstimate();
        token = document.getElementById('old-estimate-id').value
        console.log(token);
        if (token === "") {
            $("#loadError").modal("show");
        } else {
            onSampleProjectButtonClick(token);
        }
    })

    // function to load sample projects or call an old project and close modal
    function onSampleProjectButtonClick(token) {
        console.log(token);
        //get the data from the view and JSON
        $('#appendTo').nextAll('div').remove();
        $('#appendTo').nextAll('h2').remove();
        $('#appendTo').nextAll('hr').remove();
        $('#title').text("");
        var response = myModel.getOutput(token);
        var result = myModel.drawOutput(response["responseText"]);
        handleLoadResult(result);
        //   var element_to_scroll_to = document.getElementById('title');
        //   element_to_scroll_to.scrollIntoView();
    }

    function handleLoadResult(result) {
        console.log(result.get("title"));
        if (result.get("title") === "Home Repair - Fee Estimate") {
            console.log("in handleLoadResult yes Home Repair");
            var element_to_scroll_to = document.getElementById('title');
            element_to_scroll_to.scrollIntoView();
            myView.setTitle(result.get("title"));
            myView.setProjectName(result.get("projname"));
            myView.setToken(result.get("token"));
            myView.setValuation(result.get("valuation"));
            console.log("valuation from model 1st placed on page as = " + result.get("valuation"));
            myView.setExistingHabitableSquareFootage(result.get("exhabitablesqft"));
            myView.setExistingNonHabitableSquareFootage(result.get("exnonhabitablesqft"));
            myView.setNewHabitableSquareFootage(result.get("newhabitablesqft"));
            myView.setNewNonHabitableSquareFootage(result.get("newnonhabitablesqft"));
            //find out whether or not we have water fields and need to do water calculations
            myView.setNewWaterFixturesYN(result.get("newwater"));
            //if to add water fixtues if new water fixtures are being added
            if (result.get("newwater") === "Yes") {
                myView.setCurrentWaterMeter(result.get("currWaterMeter"));
                myView.setNewWaterMeter(result.get("newWaterMeter"));
                myView.setShowerTubCombo(result.get("stcombo"));
                myView.setTubOnly(result.get("tubonly"));
                myView.setShowerOnly(result.get("shower"));
                myView.setJacuzzi(result.get("jacuzzi"));
                myView.setBathSink(result.get("bathsink"));
                myView.setToilet(result.get("toilet"));
                myView.setKitchenSink(result.get("kitchensink"));
                myView.setDishwasher(result.get("dishwasher"));
                myView.setLaundrySink(result.get("laundrysink"));
                myView.setClothesWasher(result.get("clotheswasher"));
                myView.setGarageSink(result.get("garagesink"));
                myView.setBarSink(result.get("barsink"));
                myView.setHoseBib(result.get("hosebib"));
            }

            //added to call calculations for fees after loading typeform data, also doesn't recalculate valuation on 1st load to preserve a user entered valuation if there is one
            var typeOfLoad = "1st";
            actionPerformed(typeOfLoad);
        }
        // NEW DEVELOPMENT RESIDENTIAL
        else if (result.get("title") === "New Residential - Fee Estimate") {
            var element_to_scroll_to = document.getElementById('title');
            element_to_scroll_to.scrollIntoView();
            myView.setTitle(result.get("title"));
            myView.setProjectName(result.get("projname"));
            myView.setToken(result.get("token"));
            myView.setValuation(result.get("valuation"));
            console.log("type of valuation in controller = " + typeof (result.get("valuation")));
            myView.setPlannedDevelopment(result.get("plannedDevelopment"));

            //sfr values are all stored comma separated under key "sfrValues", if these values exist, use split to separate out and index number to get correct values for divs
            if (result.has("SFRValues")) {
                myView.setSFRStandalone(result.get("SFRValues").split(",")[0]);
                myView.setSFRDuplexes(result.get("SFRValues").split(",")[1]);
                myView.setSFRInLaws(result.get("SFRValues").split(",")[2]);
                myView.setSFRHabitable(result.get("SFRValues").split(",")[3]);
                myView.setSFRNonHabitable(result.get("SFRValues").split(",")[4]);
                myView.setSFR1500(result.get("SFRValues").split(",")[5]);
            }

            //apt values are all stored comma separated under key "aptValues, if these values exist, use split to separate out and index number to get correct values for divs
            if (result.has("aptValues")) {
                myView.setAptUnits(result.get("aptValues").split(",")[0]);
                //removed on switch to building centric estimator
                //myView.setAptSmallBldgs(result.get("aptValues").split(",")[1]);
                myView.setAptHabitable(result.get("aptValues").split(",")[1]);
                myView.setAptNonHabitable(result.get("aptValues").split(",")[2]);
                myView.setApt800(result.get("aptValues").split(",")[3]);
            }

            //condo values are all stored comma separated under key "condoValues", if these values exist, use split to separate out and index number to get correct values for divs
            if (result.has("condoValues")) {
                myView.setCondoUnits(result.get("condoValues").split(",")[0]);
                //removed on switch to building centric estimator
                //myView.setCondoSmallBldgs(result.get("condoValues").split(",")[1]);
                myView.setCondoHabitable(result.get("condoValues").split(",")[1]);
                myView.setCondoNonHabitable(result.get("condoValues").split(",")[2]);
                myView.setCondo800(result.get("condoValues").split(",")[3]);
            }

            //townhome values are all stored comma separated under key "townhomeValues", if these values exist, use split to separate out and index number to get correct values for divs
            if (result.has("townhomeValues")) {
                console.log("in controller townhomeValue = " + result.get("townhomeValues").split(","));
                myView.setTownhomeUnits(result.get("townhomeValues").split(",")[0]);
                myView.setTownhomeType(result.get("townhomeValues").split(",")[1]);
                myView.setTownhomeHabitable(result.get("townhomeValues").split(",")[2]);
                myView.setTownhomeNonHabitable(result.get("townhomeValues").split(",")[3]);
                myView.setTownhomeSize(result.get("townhomeValues").split(",")[4]);
            }

            //water configuration values are all stored comma separated under key "waterConfigXValues" for each water configuation
            //loop through result.get("waterConfigNumber") to then fill the field divs
            controllerWaterConfigNumber = result.get("waterConfigNumber");
            for (i = 1; i <= result.get("waterConfigNumber"); i++) {
                //create same variable as used in model to dynamically call setWaterConfigUnits for each water configuration
                var thisWaterConfig = "waterConfig" + i + "Values";
                //pass in the current water config number 'i' in order to target the correct div in setWaterConfigUnits
                myView.setWaterConfigValues(result.get(thisWaterConfig), i);
            }

            //retrieve Yes or No for inclusionary housing
            inclusionaryHousingYesOrNo = result.get("inclusionaryHousing");

            //added to call calculations for fees after loading typeform data, also doesn't recalculate valuation on 1st load to preserve a user entered valuation if there is one
            var typeOfLoad = "1st";
            actionPerformed(typeOfLoad);
        }
        // else ID not found, show modal alerting user
        // else {
        //   console.log("in if");
        // $("#loadError").modal("show");
        //}
    }

    //function to expand detailed fees sections and change text on open/close
    function onClickExpand(event) {
        //call model to find current style and switch return text other style
        var correctDisplayStyle = myModel.getDisplayStyle();
        //use value above of the new style and call model to determine correct text to use in view
        var correctText = myModel.expandOrContract(correctDisplayStyle);

        //set correct css style to open or contract (block or none)
        myView.setExpandOrContract(correctDisplayStyle);
        //set correct text
        myView.setExpandOrContractText(correctText);
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function main() {
        myView.setOnLoadListener(onloadPerformed);
        myView.setClickToExpandListener(onClickExpand);
        myView.setActionListener(actionPerformed);
    } //end main

    return {
        main: main
    }

};                                                                                                                                                                                                 //end Controller                                                               //end Controller
