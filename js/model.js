/////////returns the computation//////////////////
                var waterFieldsToAdd = [["makeTextFieldFormGroup", "current-water-meter", "Current Meter Size", "current-water-meter"],
                            ["makeTextFieldFormGroup", "new-water-meter", "Required Meter Size", "new-water-meter"],
                            ["makeNumberFieldFormGroup", "stcombo", "Shower/Tub Combo", "0", "50", "stcombo", ""],
                            ["makeNumberFieldFormGroup", "tub-only", "Bathtub alone", "0", "50", "tub-only", ""],
                            ["makeNumberFieldFormGroup", "jacuzzi", "Jacuzzi tub", "0", "50", "jacuzzi", ""],
                            ["makeNumberFieldFormGroup", "shower-only", "Shower alone", "0", "50", "shower-only", ""],
                            ["makeNumberFieldFormGroup", "bath-sink", "Bathroom Sinks", "0", "50", "bath-sink", ""],
                            ["makeNumberFieldFormGroup", "toilets", "Toilets", "0", "50", "toilet", ""],
                            ["makeNumberFieldFormGroup", "kitchen-sink", "Kitchen Sinks", "0", "50", "kitchen-sink", ""],
                            ["makeNumberFieldFormGroup", "dishwasher", "Dishwashers", "0", "50", "dishwasher", ""],
                            ["makeNumberFieldFormGroup", "laundry-sink", "Laundry Sinks", "0", "50", "laundry-sink", ""],
                            ["makeNumberFieldFormGroup", "clothes-washer", "Clothes Washers", "0", "50", "clothes-washer", ""],
                            ["makeNumberFieldFormGroup", "garage-sink", "Garage Sinks", "0", "50", "garage-sink", ""],
                            ["makeNumberFieldFormGroup", "bar-sink", "Bar Sinks", "0", "50", "bar-sink", ""],
                            ["makeNumberFieldFormGroup", "hose-bib", "Hose Bibs", "0", "50", "hose-bib", ""]
                            ];

                var Model = function () {

                    function getOutput(token) {
                        var req = false;
                        try {
                            // most browsers
                            req = new XMLHttpRequest();
                        } catch (e) {
                            // IE
                            try {
                                req = new ActiveXObject("Msxml2.XMLHTTP");
                            } catch (e) {
                                // try an older version
                                try {
                                    req = new ActiveXObject("Microsoft.XMLHTTP");
                                } catch (e) {
                                    return false;
                                }
                            }
                        }
                        if (!req) return false;
                        if (typeof success != 'function') success = function () { };
                        if (typeof error != 'function') error = function () { };
                        req.onreadystatechange = function () {
                            if (req.readyState == 4) {
                                return req.status === 200 ?
               success(req.responseText) : error(req.status);
                            }
                        }

                        var url = "php/returnJSON.php?token=".concat(token);
                        console.log(url);
                        req.open("GET", url, false);
                        req.send(null);
                        console.log(req);
                        return req;
                    }

                    ////////////////////////////////////
                    //*****DIV CREATION FUNCTIONS*****//
                    ////////////////////////////////////

                    //MABYE MOVE DIV CREATION TO VIEW, COULDN'T FIGURE IT OUT ON FIRST PASS -tim//
                    //Make a boostrap div, pass in the size and return the div with correct bootstrap size (i.e. col-md-3, col-xs-12)
                    //removed makeBootstrapDiv for now. Too generic, not needed currently
                    /* function makeBootstrapDiv(size) {
                    var bootstrapDiv = document.createElement("div")
                    var thisClass = "col-" + size;
                    bootstrapDiv.setAttribute("class", thisClass);
                    return bootstrapDiv;
                    }*/

                    // make a label and form field for number input
                    function makeNumberFieldFormGroup(labelfor, label, min, max, id, disabled) {
                        var numberFieldFormGroup = ('<div class="col-sm-6 form-group"><div class="col-md-6 col-sm-6"><label for="' + labelfor + '" class="control-label">' + label + '</label></div><div class="col-md-6 col-sm-6"><input type="number" min="' + min + '" max="' + max + '" class="form-control" id="' + id + '" ' + disabled + '></div></div>');
                        $('#mainForm').append(numberFieldFormGroup);

                    }

                    // make a label and form field for a disbled number input. Couldn't figure out passing in disabled
                    function makeDisabledNumberFieldFormGroup(labelfor, label, min, max, id, disabled) {
                        var numberFieldFormGroup = ('<div class="col-sm-6 form-group"><div class="col-md-6 col-sm-6"><label for="' + labelfor + '" class="control-label">' + label + '</label></div><div class="col-md-6 col-sm-6"><input type="number" min="' + min + '" max="' + max + '" class="form-control" id="' + id + '" + disabled></div></div>');
                        $('#mainForm').append(numberFieldFormGroup);

                    }

                    // make a label and disabled form field for a text value
                    function makeTextFieldFormGroup(labelfor, label, id) {
                        var numberFieldFormGroup = ('<div class="col-sm-6 form-group"><div class="col-md-6 col-sm-6"><label for="' + labelfor + '" class="control-label">' + label + '</label></div><div class="col-md-6 col-sm-6"><input type="text" class="form-control" id="' + id + '" disabled></div></div>');
                        $('#mainForm').append(numberFieldFormGroup);

                    }

                    // make horizontal line to separate sections
                    function makeHorizontalRule() {
                        var horizontalRule = ('<hr style="clear: both;">');
                        $('#mainForm').append(horizontalRule);

                    }

                    // make line break ?? not working right. It shows up in the html but doesn't move next stuff down to next line.
                    function makeBR() {
                        var lineBreak = ('<br />');
                        $('#mainForm').append(lineBreak);

                    }

                    // make a title
                    function makeSectionTitle(title) {
                        var sectionTitle = ('<h2 class="text-left section-title">' + title + '</h2>');
                        $('#mainForm').append(sectionTitle);
                    }

                    //function to make a set of divs. Can pass in an array with function and all div creation parameters
                    //removed this from drawOutput so it can be reused throughout app
                    function makeAllTheDivs(length, params) {
                        // loop through array and make all fields and add to main landing page
                        for (i = 0; i < length; i++) {
                            //if to determine what type of field div to make
                            if (params[i][0] === "makeNumberFieldFormGroup") {
                                var currentParameters = params[i];
                                makeNumberFieldFormGroup(currentParameters[1], currentParameters[2], currentParameters[3], currentParameters[4], currentParameters[5], currentParameters[6]);
                            }
                            if (params[i][0] === "makeDisabledNumberFieldFormGroup") {
                                var currentParameters = params[i];
                                makeDisabledNumberFieldFormGroup(currentParameters[1], currentParameters[2], currentParameters[3], currentParameters[4], currentParameters[5], currentParameters[6]);
                            }
                            else if (params[i][0] === "makeTextFieldFormGroup") {
                                var currentParameters = params[i];
                                makeTextFieldFormGroup(currentParameters[1], currentParameters[2], currentParameters[3]);
                            }
                            else if (params[i][0] === "makeHorizontalRule") {
                                makeHorizontalRule();
                            }
                            else if (params[i][0] === "makeSectionTitle") {
                                var currentParameters = params[i];
                                makeSectionTitle(currentParameters[1]);
                            }
                        }
                    }

                    //function to make the Water divs. Needed to do this differently to repeat for each section
                    //removed this from drawOutput so it can be reused throughout app
                    function makeAllTheWaterDivs(waterData) {
                        // loop through array and make all fields and add to main landing page
                        for (i = 0; i < waterData.length; i++) {
                            var params = waterData[i];
                            for (j = 0; j < params.length; j++) {
                                //if to determine what type of field div to make
                                if (params[j][0] === "makeNumberFieldFormGroup") {
                                    var currentParameters = params[j];
                                    makeNumberFieldFormGroup(currentParameters[1], currentParameters[2], currentParameters[3], currentParameters[4], currentParameters[5], currentParameters[6]);
                                }
                                else if (params[j][0] === "makeTextFieldFormGroup") {
                                    var currentParameters = params[j];
                                    makeTextFieldFormGroup(currentParameters[1], currentParameters[2], currentParameters[3]);
                                }
                                else if (params[j][0] === "makeHorizontalRule") {
                                    makeHorizontalRule();
                                }
                                else if (params[j][0] === "makeInvisibleHorizontalRule") {
                                    makeHorizontalRule();
                                }
                                else if (params[j][0] === "makeBR") {
                                    makeBR();
                                }
                                else if (params[j][0] === "makeSectionTitle") {
                                    var currentParameters = params[j];
                                    makeSectionTitle(currentParameters[1]);
                                }
                            }
                        }
                    }
                    /////////////////////////////////////////
                    //***** END DIV CREATION FUNCTIONS*****//
                    /////////////////////////////////////////

                    function drawOutput(responseText) {
                        var n = responseText.search("failed to open stream");
                        console.log(n);
                        if (n > 0) {
                            $("#loadError").modal("show");
                            // alert("ID not found, Please insert a valid ID!");
                            return false;
                        }
                        var x = (eval("(" + responseText + ")"));
                        // var x = responseText;
                        //var result = [];
                        var result = new Map();
                        var sfrhabitablesqft, townhomehabitablesqft, condohabitablesqft, apthabitablesqft, sfrnonhabitablesqft, townhomenonhabitablesqft, condononhabitablesqft, aptnonhabitablesqft;
                        var sfrModelStandalone, sfrModelDuplex, sfrModelGranny;
                        var inclusionaryHousingFee;
                        var totalMeterCost;
                        var totalRemoteReadingFee;
                        console.log(x);
                        console.log(x['xprojtype']);

                        //***** HOME REPAIR *****//
                        // three variables are Construction Type, International Building Code Group, Square Footage
                        if (x['xprojtype'] === "Home Remodels") {
                            console.log("in if to make divs home repair");
                            //make arrway of values to pass for div and field creation on the fly, [0] value is what form creation div function to call since we will have different types (number, text, etc)
                            var fieldsToAdd = [["makeTextFieldFormGroup", "project-name", "Project Name", "project-name"],
                            ["makeDisabledNumberFieldFormGroup", "valuation", "Valuation", "0", "5000000000", "valuation", ""],
                            ["makeNumberFieldFormGroup", "existing-habitable-squarefootage", "Existing Habitable Sq Ft", "0", "5000000000", "existing-habitable-squarefootage"],
                            ["makeNumberFieldFormGroup", "existing-nonhabitable-squarefootage", "Existing Non-Habitable Sq Ft", "0", "5000000000", "existing-nonhabitable-squarefootage"],
                            ["makeNumberFieldFormGroup", "new-habitable-squarefootage", "New Habitable Sq Ft", "0", "5000000000", "new-habitable-squarefootage"],
                            ["makeNumberFieldFormGroup", "new-nonhabitable-squarefootage", "New Non-Habitable Sq Ft", "0", "5000000000", "new-nonhabitable-squarefootage"],
                            ["makeTextFieldFormGroup", "new-water-yes-or-no", "New Water Fixtures", "new-water-yes-or-no"]];

                            //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                            makeAllTheDivs(fieldsToAdd.length, fieldsToAdd);

                            // if new water = yes (1) then assign yes, if no, assign no; typeform uses 0 and 1
                            if (x['hr-newwateryesorno'] === "1") {
                                console.log("in if for newwateryesorno");
                                console.log("hr-newwateryesorno = " + x['hr-newwateryesorno']);
                                var newwateryesorno = "Yes";

                                var newWaterMeter = figureResidentialWaterMeter(parseInt(x['hr-stcombo']),
                                                                 parseInt(x['hr-tubonly']),
                                                                 parseInt(x['hr-jacuzzi']),
                                                                 parseInt(x['hr-shower']),
                                                                 parseInt(x['hr-bathsink']),
                                                                 parseInt(x['hr-toilet']),
                                                                 parseInt(x['hr-kitchensink']),
                                                                 parseInt(x['hr-dishwasher']),
                                                                 parseInt(x['hr-laundrysink']),
                                                                 parseInt(x['hr-clotheswasher']),
                                                                 parseInt(x['hr-garagesink']),
                                                                 parseInt(x['hr-barsink']),
                                                                 parseInt(x['hr-hosebib']));
                                result.set("currWaterMeter", x['hr-currentwatermeter']);
                                result.set("newWaterMeter", newWaterMeter);
                                result.set("stcombo", x['hr-stcombo']);
                                result.set("tubonly", x['hr-tubonly']);
                                result.set("shower", x['hr-shower']);
                                result.set("jacuzzi", x['hr-jacuzzi']);
                                result.set("bathsink", x['hr-bathsink']);
                                result.set("toilet", x['hr-toilet']);
                                result.set("kitchensink", x['hr-kitchensink']);
                                result.set("dishwasher", x['hr-dishwasher']);
                                result.set("laundrysink", x['hr-laundrysink']);
                                result.set("clotheswasher", x['hr-clotheswasher']);
                                result.set("garagesink", x['hr-garagesink']);
                                result.set("barsink", x['hr-barsink']);
                                result.set("hosebib", x['hr-hosebib']);

                                //call function to make all the divs for waterFieldsToAdd variable, pass in length of array and all parameters for div creation
                                makeAllTheDivs(waterFieldsToAdd.length, waterFieldsToAdd);
                            } else {
                                var newwateryesorno = "No";
                            }

                            //assign square footages to be used in view and to create valuations if needed
                            if (x['hr-exhabitablesqft']) { result.set("exhabitablesqft", x['hr-exhabitablesqft']); } else { result.set("exhabitablesqft", 0); }
                            if (x['hr-exnonhabitablesqft']) { result.set("exnonhabitablesqft", x['hr-exnonhabitablesqft']); } else { result.set("exnonhabitablesqft", 0); }
                            if (x['hr-newhabitablesqft']) { result.set("newhabitablesqft", x['hr-newhabitablesqft']); } else { result.set("newhabitablesqft", 0); }
                            if (x['hr-newnonhabitablesqft']) { result.set("newnonhabitablesqft", x['hr-newnonhabitablesqft']); } else { result.set("newnonhabitablesqft", 0); }

                            //if user entered valuation, assign it to var valuation
                            if (x['hr-valuation']) {
                                var valuation = x['hr-valuation'];
                                console.log(typeof (valuation) + " in known valuation home repair");
                            }
                            //else if user did not enter valuation, then call getValuation passing (VB (construction type for home repair), "Residential", and Square footage entered
                            else {
                                console.log(result.get('exhabitablesqft') + " " + result.get('exnonhabitablesqft') + " " + result.get('newhabitablesqft') + " " + result.get('newnonhabitablesqft'));
                                var valuation1 = getValuation("VB", "Residential, one- and two-family", (parseInt(result.get('newhabitablesqft')))) + getValuation("VB", "Utility, miscellaneous", (parseInt(result.get('newnonhabitablesqft'))));
                                var valuation2 = (getValuation("VB", "Residential, one- and two-family", (parseInt(result.get('exhabitablesqft')))) * 0.6) + (getValuation("VB", "Utility, miscellaneous", (parseInt(result.get('exnonhabitablesqft')))) * 0.6);
                                console.log("valuation calculation = " + valuation1);
                                console.log("valuation calculation = " + valuation2);
                                var valuation = valuation1 + valuation2;
                                console.log(valuation);
                            }

                            result.set("title", "Home Repair - Fee Estimate");
                            result.set("projname", x['xprojname']);
                            result.set("token", x['xtoken']);
                            result.set("valuation", parseInt(valuation).toFixed(2));
                            result.set("newwater", newwateryesorno);
                        }

                        //***** NEW DEVELOPMENT *****//
                        // If Project selected is New Development
                        if (x['xprojtype'] === "New Development") {
                            console.log("in if to make div for new development");
                            //make arrway of values to pass for div and field creation on the fly, [0] value is what form creation div function to call since we will have different types (number, text, etc)
                            var initialFieldsToAdd = [["makeTextFieldFormGroup", "project-name", "Project Name", "project-name"],
                            //["makeTextFieldFormGroup", "valuation", "Valuation", "valuation "],
                            ["makeDisabledNumberFieldFormGroup", "valuation", "Valuation", "1", "5000000000", "valuation", ""],
                            ["makeTextFieldFormGroup", "planned-development", "Planned Development", "planned-development"],
                            ["makeHorizontalRule"]];

                            //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                            makeAllTheDivs(initialFieldsToAdd.length, initialFieldsToAdd);

                            //make SFR divs
                            if (x['newdev-unittype'] === "Single Family Home") {
                                var sfrFieldsToAdd = [["makeSectionTitle", "Single Family Homes"],
                                //removed for building centric survey      ["makeNumberFieldFormGroup", "sfr-units", "Single Family Units", "0", "5000000000", "sfr-units", ""],
                                    ["makeNumberFieldFormGroup", "sfr-standalone", "Standalone Units", "0", "5000000000", "sfr-standalone", ""],
                                    ["makeNumberFieldFormGroup", "sfr-duplexes", "Duplex Units", "0", "5000000000", "sfr-duplexes", ""],
                                    ["makeNumberFieldFormGroup", "sfr-inlaw", "In-law Units", "0", "5000000000", "sfr-inlaw", ""],
                                    ["makeNumberFieldFormGroup", "sfr-habitablesqft", "Total Habitable Sq Ft", "0", "5000000000", "sfr-habitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "sfr-nonhabitablesqft", "Total Non-Habitable Sq Ft", "0", "5000000000", "sfr-nonhabitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "sfr-1500sqft", "Units > 1500 sq ft", "0", "5000000000", "sfr-1500sqft", ""],
                                        ["makeHorizontalRule"]];

                                //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                                makeAllTheDivs(sfrFieldsToAdd.length, sfrFieldsToAdd);

                                switch (x['newdev-sfrtype']) {
                                    case "One new detached single family home":
                                        sfrModelStandalone = 1;
                                        sfrModelDuplex = 0;
                                        sfrModelGranny = 0;
                                        break;
                                    case "One new detached single family home with a new in-law unit":
                                        sfrModelStandalone = 1;
                                        sfrModelDuplex = 0;
                                        sfrModelGranny = 1;
                                        break;
                                    case "One new duplex":
                                        sfrModelStandalone = 0;
                                        sfrModelDuplex = 2;
                                        sfrModelGranny = 0;
                                        break;
                                    case "One new in-law unit attached to an existing detached single family home":
                                        sfrModelStandalone = 0;
                                        sfrModelDuplex = 0;
                                        sfrModelGranny = 1;
                                        break;
                                }
                                //store all values for sfr divs under one key, "SFRValues", 3rd value is duplexes and is derived from total - standalone - in laws
                                result.set("SFRValues", sfrModelStandalone + "," + sfrModelDuplex + "," + sfrModelGranny + "," + x['newres-sfrhabitablesqft'] + "," + x['newres-sfrnonhabitablesqft'] + "," + x['newres-sfr1500']);
                            }

                            //process townhome data
                            if (x['newdev-unittype'] === "Townhomes") {

                                //if these townhomes are each on individual lots, we assign a different label to the townhome-size div, and assign the appropriate value to townhomeValues key in result
                                if (x['newres-townhometype'] === "Individual Lots") {

                                    var townhometype = "Units > 1500 sq ft";
                                    //set all values for townhome divs under one key, "townhomeValues", including the appropriate value for townhome-size
                                    result.set("townhomeValues", x['newres-townhomeunits'] + "," + x['newres-townhometype'] + "," + x['newres-townhomehabitablesqft'] + "," + x['newres-townhomenonhabitablesqft'] + "," + x['newres-sfrtownhome1500']);
                                }
                                //else these townhomes are on a group lot, we assign a different label to the townhome-size div, and assign the appropriate value to townhomeValues key in result
                                else {
                                    var townhometype = "Units > 800 sq ft";
                                    //set all values for townhome divs under one key, "townhomeValues", including the appropriate value for townhome-size
                                    result.set("townhomeValues", x['newres-townhomeunits'] + "," + x['newres-townhometype'] + "," + x['newres-townhomehabitablesqft'] + "," + x['newres-townhomenonhabitablesqft'] + "," + x['newres-multitownhome800']);
                                }
                                var townhomeFieldsToAdd = [["makeSectionTitle", "Townhomes"],
                                    ["makeNumberFieldFormGroup", "townhome-units", "Townhome Units", "0", "5000000000", "townhome-units", ""],
                                     ["makeTextFieldFormGroup", "townhome-type", "Townhome Type", "townhome-type"],
                                    ["makeNumberFieldFormGroup", "townhome-habitablesqft", "Total Habitable Sq Ft", "0", "5000000000", "townhome-habitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "townhome-nonhabitablesqft", "Total Non-Habitable Sq Ft", "0", "5000000000", "townhome-nonhabitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "townhome-size", townhometype, "0", "5000000000", "townhome-size", ""],
                                        ["makeHorizontalRule"]];

                                //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                                makeAllTheDivs(townhomeFieldsToAdd.length, townhomeFieldsToAdd);

                            }

                            //process condo data
                            if (x['newdev-unittype'] === "Condos") {
                                var condoFieldsToAdd = [["makeSectionTitle", "Condos"],
                                    ["makeNumberFieldFormGroup", "condo-units", "Condo Units", "0", "5000000000", "condo-units", ""],
                                //removed for building centric survey, we automatically know how many units in buildings > 5 units because each estimate here is 1 building
                                // ["makeNumberFieldFormGroup", "condo-smallbuildings", "Condos in Buildings with < 5 units", "0", "5000000000", "condo-smallbuildings", ""],
                                    ["makeNumberFieldFormGroup", "condo-habitablesqft", "Total Habitable Sq Ft", "0", "5000000000", "condo-habitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "condo-nonhabitablesqft", "Total Non-Habitable Sq Ft", "0", "5000000000", "condo-nonhabitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "condo-800sqft", "Units > 800 sq ft", "0", "5000000000", "condo-800sqft", ""],
                                        ["makeHorizontalRule"]];

                                //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                                makeAllTheDivs(condoFieldsToAdd.length, condoFieldsToAdd);

                                //store all values for condo divs under one key, "condoValues"
                                result.set("condoValues", x['newres-condounits'] + "," + x['newres-condohabitablesqft'] + "," + x['newres-condononhabitablesqft'] + "," + x['newres-condo800']);
                            }

                            //process apt data
                            if (x['newdev-unittype'] === "Apartments") {
                                var apartmentFieldsToAdd = [["makeSectionTitle", "Apartments"],
                                    ["makeNumberFieldFormGroup", "apt-units", "Apartment Units", "0", "5000000000", "apt-units", ""],
                                //removed for building centric survey, we automatically know how many units in buildings > 5 units because each estimate here is 1 building
                                // ["makeNumberFieldFormGroup", "apt-smallbuildings", "Apartments in Buildings with < 5 units", "0", "5000000000", "apt-smallbuildings", ""],
                                    ["makeNumberFieldFormGroup", "apt-habitablesqft", "Total Habitable Sq Ft", "0", "5000000000", "apt-habitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "apt-nonhabitablesqft", "Total Non-Habitable Sq Ft", "0", "5000000000", "apt-nonhabitablesqft", ""],
                                    ["makeNumberFieldFormGroup", "apt-800sqft", "Units > 800 sq ft", "0", "5000000000", "apt-800sqft", ""],
                                        ["makeHorizontalRule"]];
                                console.log(apartmentFieldsToAdd[0]);
                                //call function to make all the divs for fieldsToAdd variable, pass in length of array and all parameters for div creation
                                makeAllTheDivs(apartmentFieldsToAdd.length, apartmentFieldsToAdd);

                                //store all values for apt divs under one key, "aptValues"
                                result.set("aptValues", x['newres-aptunits'] + "," + x['newres-apthabitablesqft'] + "," + x['newres-aptnonhabitablesqft'] + "," + x['newres-apt800']);
                            }

                            //make water divs by looping through all configurations
                            //make custom IDs for all divs, so we can find/change values later

                            var waterConfigs = x['wc'];
                            var allResidentialWaterDivs = [];
                            for (i = 1; i <= waterConfigs.length; i++) {
                                var waterDivIDS = [];
                                waterDivIDS.push("wc" + i + "-units");
                                waterDivIDS.push("wc" + i + "-watermeter");
                                waterDivIDS.push("wc" + i + "-stcombo");
                                waterDivIDS.push("wc" + i + "-tub-only");
                                waterDivIDS.push("wc" + i + "-jacuzzi");
                                waterDivIDS.push("wc" + i + "-shower-only");
                                waterDivIDS.push("wc" + i + "-bath-sink");
                                waterDivIDS.push("wc" + i + "-toilets");
                                waterDivIDS.push("wc" + i + "-kitchen-sink");
                                waterDivIDS.push("wc" + i + "-dishwasher");
                                waterDivIDS.push("wc" + i + "-laundry-sink");
                                waterDivIDS.push("wc" + i + "-clothes-washer");
                                waterDivIDS.push("wc" + i + "-garage-sink");
                                waterDivIDS.push("wc" + i + "-bar-sink");
                                waterDivIDS.push("wc" + i + "-hose-bib");

                                var sectionTitle = "Water Configuration " + i;
                                console.log(waterDivIDS[0]);

                                //new development residential water fields
                                var newdevResidentialWaterFieldsToAdd = [["makeSectionTitle", sectionTitle],
                                    ["makeNumberFieldFormGroup", waterDivIDS[0], "Units", "0", "500000", waterDivIDS[0], ""],
                                    ["makeTextFieldFormGroup", waterDivIDS[1], "Water Meter Size", waterDivIDS[1]],
                                    ["makeNumberFieldFormGroup", waterDivIDS[2], "Shower/Tub Combo", "0", "50", waterDivIDS[2], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[3], "Tub Only", "0", "50", waterDivIDS[3], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[4], "Jacuzzi Tub", "0", "50", waterDivIDS[4], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[5], "Shower Stalls", "0", "50", waterDivIDS[5], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[6], "Bathroom Sinks", "0", "50", waterDivIDS[6], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[7], "Toilets", "0", "50", waterDivIDS[7], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[8], "Kitchen Sinks", "0", "50", waterDivIDS[8], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[9], "Dishwashers", "0", "50", waterDivIDS[9], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[10], "Laundry Sinks", "0", "50", waterDivIDS[10], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[11], "Clothes Washers", "0", "50", waterDivIDS[11], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[12], "Garage Sinks", "0", "50", waterDivIDS[12], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[13], "Bar Sinks", "0", "50", waterDivIDS[13], ""],
                                    ["makeNumberFieldFormGroup", waterDivIDS[14], "Hose Bibs", "0", "50", waterDivIDS[14], ""],
                                    ["makeHorizontalRule"]
                                ];

                                //push all current div make requests for this i into the total holding array
                                //couldn't make divs as we went with each i, maybe have been async issue
                                //when done processing all the different water configs, make the divs outside the for loop with this array
                                allResidentialWaterDivs.push(newdevResidentialWaterFieldsToAdd);
                                console.log(allResidentialWaterDivs);
                                console.log(allResidentialWaterDivs.length);

                                var newWaterMeterArray = [];
                                var thisMeter = figureResidentialWaterMeter(waterConfigs[i - 1][1],
                                    waterConfigs[i - 1][2],
                                    waterConfigs[i - 1][3],
                                    waterConfigs[i - 1][4],
                                    waterConfigs[i - 1][5],
                                    waterConfigs[i - 1][6],
                                    waterConfigs[i - 1][7],
                                    waterConfigs[i - 1][8],
                                    waterConfigs[i - 1][9],
                                    waterConfigs[i - 1][10],
                                    waterConfigs[i - 1][11],
                                    waterConfigs[i - 1][12],
                                    waterConfigs[i - 1][13]);


                                // create variable to use as key when storing water config values for each water configuration
                                var thisWaterConfig = "waterConfig" + i + "Values";
                                // store all water config values for passing to model, comma separated for storage
                                result.set(thisWaterConfig, waterConfigs[i - 1][0] + "," + waterConfigs[i - 1][1] + "," + waterConfigs[i - 1][2] + "," + waterConfigs[i - 1][3] + "," + waterConfigs[i - 1][4]
                                     + "," + waterConfigs[i - 1][5] + "," + waterConfigs[i - 1][6] + "," + waterConfigs[i - 1][7] + "," + waterConfigs[i - 1][8] + "," + waterConfigs[i - 1][9] + "," + waterConfigs[i - 1][10]
                                      + "," + waterConfigs[i - 1][1] + "," + waterConfigs[i - 1][12] + "," + waterConfigs[i - 1][13] + "," + thisMeter);

                            } //end for to generate water variables and water div variables. I think I have to call them seperately
                            //only one set was being created if I made them inside the for loop. Maybe needs to be asynch

                            makeAllTheWaterDivs(allResidentialWaterDivs);


                            //NEW DEV RESIDENTIAL VALUATION
                            if (x['newres-valuation']) {
                                var valuation = x['newres-valuation'];
                                console.log(typeof (valuation));
                            }
                            //else if user did not enter valuation, then call getValuation passing (VB (construction type for home repair), "Residential", and Square footage entered
                            else {
                                //check if a unit type (sft, townhome, condo, apt) has sq ft, if not assign 0 to this type
                                if (x['newres-sfrhabitablesqft']) { sfrhabitablesqft = x['newres-sfrhabitablesqft']; } else { sfrhabitablesqft = 0; }
                                if (x['newres-sfrnonhabitablesqft']) { sfrnonhabitablesqft = x['newres-sfrnonhabitablesqft']; } else { sfrnonhabitablesqft = 0; }
                                console.log("sfrhabitablesqft = " + sfrhabitablesqft);
                                console.log("sfrnonhabitablesqft = " + sfrnonhabitablesqft);
                                // townhome square footages
                                if (x['newres-townhomehabitablesqft']) {
                                    townhomehabitablesqft = x['newres-townhomehabitablesqft'];
                                    //check to see if townhomes are indivual or group lots which dictates building type/cost, pass these values to getValuation below
                                    if (x['newres-townhometype'] === "Individual Lots") {
                                        var townhomeBldgGroup = "Residential, one- and two-family";
                                        var townhomeBldgType = "VB";
                                    } else {
                                        var townhomeBldgGroup = "Residential, multiple family";
                                        var townhomeBldgType = "VB";
                                    }
                                } else {
                                    townhomehabitablesqft = 0;
                                    //need to assign these variables values that will make getValuation work, even though there are no townhome units
                                    //we use these values when there are townhomes to either calculate sfr townhomes or multi family townhomes
                                    var townhomeBldgGroup = "Residential, one- and two-family";
                                    var townhomeBldgType = "VB";
                                }
                                if (x['newres-townhomenonhabitablesqft']) { townhomenonhabitablesqft = x['newres-townhomenonhabitablesqft']; } else { townhomenonhabitablesqft = 0; }

                                // condo square footages
                                if (x['newres-condohabitablesqft']) { condohabitablesqft = x['newres-condohabitablesqft']; } else { condohabitablesqft = 0; }
                                if (x['newres-condononhabitablesqft']) { condononhabitablesqft = x['newres-condononhabitablesqft']; } else { condononhabitablesqft = 0; }

                                // apartment square footages
                                if (x['newres-apthabitablesqft']) { apthabitablesqft = x['newres-apthabitablesqft']; } else { apthabitablesqft = 0; }
                                if (x['newres-aptnonhabitablesqft']) { aptnonhabitablesqft = x['newres-aptnonhabitablesqft']; } else { aptnonhabitablesqft = 0; }

                                console.log("townhomeBldgType = " + townhomeBldgType);
                                console.log("townhomeBldgGroup = " + townhomeBldgGroup);
                                console.log("townhomeHabitableSqFt = " + townhomehabitablesqft);
                                console.log("townhomenonHabitableSqFt = " + townhomenonhabitablesqft);
                                //call getNewResValuation using all the various square footages

                                var valuation = (getValuation("VB", "Residential, one- and two-family", sfrhabitablesqft) +
                                                getValuation("VB", "Utility, miscellaneous", sfrnonhabitablesqft) +
                                                getValuation(townhomeBldgType, townhomeBldgGroup, townhomehabitablesqft) +
                                                getValuation("VB", "Utility, miscellaneous", townhomenonhabitablesqft) +
                                                getValuation("VB", "Residential, multiple family", condohabitablesqft) +
                                                getValuation("VB", "Utility, miscellaneous", condononhabitablesqft) +
                                                getValuation("VB", "Residential, multiple family", apthabitablesqft) +
                                                getValuation("VB", "Utility, miscellaneous", aptnonhabitablesqft)).toFixed(2);

                                console.log("valuation calculation = " + valuation);
                            }
                            console.log("valuation = " + valuation);
                            console.log(typeof (valuation));

                            //END NEW DEV RESIDENTIAL VALUATION

                            //NEW DEV INCLUSIONARY HOUSING
                            if (x['newres-housinginclusionary']) {
                                // if variable exists and value from typeform is 1 (1 = yes on typeform questions), then Fee = Yes
                                if (x['newres-housinginclusionary'] === "1") { inclusionaryHousing = "Yes"; }
                                // else Fee = No, total project units < 20
                                else { inclusionaryHousing = "No"; }
                            } else
                            //if no inclusionary housing variable from typeform, then Fee = Yes because the building entered has >= 20 units and we didn't even ask the inclusionary housing question
                            {
                                inclusionaryHousing = "Yes";
                            }
                            //END NEW DEV INCLUSIONARY HOUSING


                            result.set("title", "New Residential - Fee Estimate");
                            result.set("projname", x['xprojname']);
                            result.set("token", x['xtoken']);
                            //result.set("valuation", valuation.toFixed(2));
                            result.set("valuation", valuation);
                            result.set("inclusionaryHousing", inclusionaryHousing);
                            if (x['newres-pd'] === "0") {
                                result.set("plannedDevelopment", "No");
                            } else {
                                result.set("plannedDevelopment", "Yes");
                            }
                            result.set("waterConfigNumber", x['wc'].length);
                            console.log(result);
                        } //end if for new development residential

                        return result;

                    } // end DrawOuput

                    //CALCULATE NEW WATER METER SIZE IF ADDITIONAL WATER FIXTURES ARE ADDED
                    //derive fixture count and return required water meter size
                    function figureResidentialWaterMeter(stcombo, tub, jacuzzi, shower, bathsink, toilet, kitsink, dishwasher, laundrysink, clothes, garagesink, barsink, hosebib) {
                        //perform fixture counts (Meter Sizing Calculation for Residential spreadsheet)
                        var hoseBibUnitCount = (1 * 2.5) + (hosebib - 1) * 1;
                        console.log(hosebib);
                        console.log(stcombo, " ", tub, " ", jacuzzi, " ", shower, " ", bathsink, " ", toilet, " ", kitsink, " ", dishwasher, " ", laundrysink, " ", clothes, " ", garagesink, " ", barsink, " ", hosebib);
                        var fixtureCount = (stcombo * 4) +
                            (tub * 4) +
                            (jacuzzi * 10) +
                            (shower * 2) +
                            (bathsink * 1) +
                            (toilet * 2.5) +
                            (kitsink * 1.5) +
                            (dishwasher * 1.5) +
                            (laundrysink * 1.5) +
                            (clothes * 4) +
                            (garagesink * 1.5) +
                            (barsink * 1) +
                            (hoseBibUnitCount);
                        var newMeterSize;
                        //conditional to determine which expression is true
                        switch (true) {
                            case (fixtureCount <= 23):
                                newMeterSize = "5/8\""
                                break;
                            case (fixtureCount > 23 && fixtureCount <= 40):
                                newMeterSize = "3/4\""
                                break;
                            case (fixtureCount > 40 && fixtureCount <= 85):
                                newMeterSize = "1\""
                                break
                            case (fixtureCount > 85):
                                newMeterSize = "1 1/2\""
                                break
                        }
                        console.log("Fixture Count = " + fixtureCount);
                        console.log("New Water Meter Size = " + newMeterSize);
                        return newMeterSize;
                    }

                    //CALCULATE HOME REPAIR WATER METER FEES (if upgraded meter is needed)
                    //find new meter and compare against old meter
                    function calcHomeRepairWaterFees(currM, newM) {
                        //currently home repair only
                        //create array of water meter fees
                        var residentialWaterFees = [];
                        //FEE DATA\\
                        residentialWaterFees.push({ size: "5/8\"", fee: 6484 });
                        residentialWaterFees.push({ size: "3/4\"", fee: 9730 });
                        residentialWaterFees.push({ size: "1\"", fee: 12158 });
                        residentialWaterFees.push({ size: "1 1/2\"", fee: 32420 });
                        var waterFees = [];

                        //run loop to get the existing meter's cost
                        var match = "N";
                        var counter = 0;
                        while (match === "N") {
                            if (currM === residentialWaterFees[counter].size) {
                                var existingMeterFee = residentialWaterFees[counter].fee;
                                match = "Y";
                            } else {
                                counter += 1;
                            }
                        }

                        //run loop again to get the new meter's cost
                        var match = "N";
                        var counter = 0;
                        while (match === "N") {
                            if (newM === residentialWaterFees[counter].size) {
                                var newMeterFee = residentialWaterFees[counter].fee;
                                match = "Y";
                            } else {
                                counter += 1;
                            }
                        }
                        var remoteReadingFee = 0;

                        //if new fee is greater than old fee, the cost of the upgrade is the difference in prices
                        if (newMeterFee > existingMeterFee) {
                            var waterConnectionFee = newMeterFee - existingMeterFee;
                        }
                        //if new is less than existing (unlikely that someone would remove features or would have installed such a large meter that
                        //even when they add features, the required new meter is less than the existing
                        else {
                            var waterConnectionFee = 0;
                        }

                        waterFees[0] = waterConnectionFee;
                        waterFees[1] = remoteReadingFee;
                        console.log(waterFees[0], waterFees[1]);
                        return waterFees;
                    }

                    //CALCULATE NEW DEVELOPMENT WATER METER FEES
                    function calcNewDevelopmentWaterFees(waterFeatures) {
                        //we are passing in from the controller 'waterFeatures' which is
                        //units, stcombo, tub, jacuzzi, shower, bathsink, toilet, kitsink, dishwasher, laundrysink, clotheswasher, garagesink, barsink, hosebib, water meter
                        //create array of water meter fees
                        var residentialWaterFees = [];
                        residentialWaterFees.push({ size: "5/8\"", fee: 6484 });
                        residentialWaterFees.push({ size: "3/4\"", fee: 9730 });
                        residentialWaterFees.push({ size: "1\"", fee: 12158 });
                        residentialWaterFees.push({ size: "1 1/2\"", fee: 32420 });
                        var waterFees = [];
                        var waterMeterCost = 0;
                        var remoteReadingCost = 0;

                        for (i = 0; i < waterFeatures.length; i++) {
                            //create same variable as used in model to dynamically call setWaterConfigUnits for each water configuration

                            //run loop to get the meter's cost
                            var match = "N";
                            var counter = 0;
                            while (match === "N") {
                                if (waterFeatures[i][14] === residentialWaterFees[counter].size) {
                                    console.log("in match");
                                    //get meter fee multiplied by units from water features array index 0
                                    var meterFee = residentialWaterFees[counter].fee * waterFeatures[i][0];
                                    //get remote reading fee multiplied by units from water features array index 0
                                    var remoteReadingFee = 200 * waterFeatures[i][0];
                                    console.log("meter fee = " + meterFee);
                                    console.log("remote reading fee = " + remoteReadingFee);
                                    match = "Y";
                                } else {
                                    counter += 1;
                                }
                            }

                            waterMeterCost = waterMeterCost + meterFee;
                            remoteReadingCost = remoteReadingCost + remoteReadingFee;

                        }

                        waterFees[0] = waterMeterCost;
                        waterFees[1] = remoteReadingCost;
                        return waterFees;
                    }

                    //CALCULATE SEWER CONNECTION FEES
                    function calcSewerConnectionFee(sewerUnits) {
                        // newdev-residentialorcommercial
                        // we are passing in array of two values, one for single family sewer connection fees; one for multi family sewer fees
                        //$7700 single family fee, $6853 for multi family fee
                        var singleFamilyFee = 7700 * sewerUnits[0];
                        var multiFamilyFee = 6853 * sewerUnits[1];

                        return singleFamilyFee + multiFamilyFee;
                    }

                    //CALCULATE VALUATION, both on initial load and when recalculating changes
                    function getValuation(constructionType, bldgCodeGroup, squareFootage) {
                        var data = {
                            "Theaters, with stage": "250.68, 240.19, 225.83, 219.32, 206.42, 198.60",
                            "Theaters, without stage": "228.45, 217.96, 203.72, 197.21, 184.31, 176.49",
                            "Restaurants, bars, banquet halls": "192.64, 186.17, 173.98, 170.26, 157.39, 153.11",
                            "Churches": "230.86, 220.38, 206.42, 199.91, 187.02, 179.20",
                            "Community halls, libraries": "189.02, 179.70, 164.41, 159.06, 145.00, 138.34",
                            "Arenas": "226.13, 216.80, 201.40, 196.05, 181.99, 175.33",
                            "Business": "197.57, 187.78, 171.16, 164.72, 150.21, 143.56",
                            "Educational": "208.97, 199.66, 186.44, 176.96, 162.93, 157.97",
                            "Residential, multiple family": "165.67, 156.11, 142.97, 138.25, 124.96, 119.97",
                            "Residential, one- and two-family": "158.35, 154.08, 148.42, 144.55, 138.89, 130.68",
                            "Residential, care": "198.33, 188.77, 174.64, 169.92, 156.62, 151.64",
                            "Storage, moderate hazard": "104.24, 99.99, 88.86, 84.44, 71.61, 66.90",
                            "Storage, low hazard": "104.24, 98.83, 88.86, 83.28, 71.61, 65.74",
                            "Utility, miscellaneous": "80.09, 76.01, 68.70, 64.16, 54.32, 51.77"
                        };
                        console.log(constructionType);
                        console.log(bldgCodeGroup);
                        console.log(squareFootage);
                        var priceIndexes = data[bldgCodeGroup];
                        console.log(priceIndexes);
                        var priceIndexList = priceIndexes.split(",");
                        var selectedPriceIndex;
                        switch (constructionType) {
                            case 'IIA':
                                selectedPriceIndex = priceIndexList[0];
                                break;
                            case 'IIB':
                                selectedPriceIndex = priceIndexList[1];
                                break;
                            case 'IIIA':
                                selectedPriceIndex = priceIndexList[2];
                                break
                            case 'IIIB':
                                selectedPriceIndex = priceIndexList[3];
                                break
                            case 'VA':
                                selectedPriceIndex = priceIndexList[4];
                                break
                            case 'VB':
                                selectedPriceIndex = priceIndexList[5];
                                break
                                //case 'base':
                                //  vals = ['Please choose from above'];
                        }
                        console.log("Price Index = " + selectedPriceIndex + " is a " + typeof (selectedPriceIndex));
                        console.log("Square Footage = " + squareFootage + " is a " + typeof (squareFootage));
                        console.log("Selected Price index = " + selectedPriceIndex);
                        var derivedValuation = selectedPriceIndex * squareFootage;
                        console.log("derived valuation in getCalculation = " + derivedValuation);
                        return derivedValuation;
                    }

                    function calcInsFee(valuation) {
                        var val = parseInt(valuation);
                        console.log(valuation);
                        var insFee;
                        if (val < 501) {
                            insFee = 29.77;
                        } else if (val > 500 && val < 2001) {
                            var overage100 = Math.ceil((val - 500) / 100);
                            insFee = 29.77 + (3.87 * overage100);
                        } else if (val > 2000 && val < 25001) {
                            var overage1000 = Math.ceil((val - 2000) / 1000);
                            insFee = 87.82 + (17.74 * overage1000);
                        } else if (val > 25000 && val < 50001) {
                            var overage1000 = Math.ceil((val - 25000) / 1000);
                            insFee = 495.68 + (12.80 * overage1000);
                        } else if (val > 50000 && val < 100001) {
                            var overage1000 = Math.ceil((val - 50000) / 1000);
                            insFee = 815.70 + (8.87 * overage1000);
                        } else if (val > 100000 && val < 500001) {
                            var overage1000 = Math.ceil((val - 100000) / 1000);
                            insFee = 1259.15 + (7.09 * overage1000);
                        } else if (val > 500000 && val < 1000001) {
                            var overage1000 = Math.ceil((val - 500000) / 1000);
                            insFee = 4097.18 + (6.02 * overage1000);
                        } else if (val > 1000000) {
                            var overage1000 = Math.ceil((val - 1000000) / 1000);
                            insFee = 7109.14 + (4.00 * overage1000);
                        }

                        console.log("insFee = " + insFee);
                        console.log("insFee fixed 2 = " + insFee.toFixed(2));
                        console.log("parse flotat insFree fixed 2 = " + parseFloat(insFee.toFixed(2)));

                        return parseFloat(insFee.toFixed(2));
                    }

                    //INSPECTION FEES
                    function calcBldgInspectionFee(insFee) {
                        return insFee;
                    }
                    //calc fire fee if needed
                    function calcFireInspectionFee() {
                        return 221;
                    }
                    //calc hazmat fee if needed
                    function calcHazmatInspectionFee() {
                        return 0;
                    }
                    //don't need Building Plan Check Fee, just copy Building Inspection Fee in view.js

                    //PLAN REVIEW FEES

                    //Building Plan Check Fee = Building Inspection Fee * 1.00
                    function calcBuildingPlanCheckFee(insFee) {
                        return (insFee * 1.0).toFixed(2);
                    }

                    //Fire Review Fee = Building Inspection Fee * 0.35
                    function calcFireReviewFee(insFee) {
                        return (insFee * 0.35).toFixed(2);
                    }

                    //Planning Review Fee = Building Inspection Fee * 0.35
                    function calcPlanningReviewFee(insFee) {
                        return (insFee * 0.35).toFixed(2);
                    }
                    function calcSolidWasteReviewFee() {
                        return 80;
                    }

                    //ADMINISTRATIVE FEES
                    //Tecnology Fee = Building Inspection Fee * 0.03
                    function calcTechnologyFee(insFee) {
                        return (insFee * 0.03).toFixed(2);
                    }

                    //Community Planning Fee = Building Inspection Fee * 0.16
                    function calcCommunityPlanningFee(insFee) {
                        return (insFee * 0.16).toFixed(2);
                    }
                    function calcPermitIssuanceFee() {
                        return 147;
                    }

                    //SMIP Fee residential
                    function calcSMIPFee(valuation) {
                        return valuation * 0.00013;
                    }

                    // California Building Standards Fee
                    function calcBuildingStandardsFee(valuation) {
                        console.log(valuation);
                        console.log("Bldg Standards value/25 + 1 = " + Math.floor(valuation / 25000 + 1));
                        var buildingStandardsFee = Math.floor(valuation / 25000) + 1;
                        return buildingStandardsFee;
                    }

                    //ADDITIONAL FEES & TAXES

                    //calc school district fees
                    function calcHomeRepairSchoolDistrictFee(newsqft) {
                        if (newsqft > 500) {
                            var schoolFee = 2.97 * newsqft;
                        } else {
                            var schoolFee = 0;
                        }
                        console.log("School Fee = " + schoolFee);
                        console.log("school fee type in model = " + schoolFee);
                        return schoolFee;
                    }

                    //calc school district fees for residential new development
                    function calcNewDevelopmentResidentialSchoolDistrictFee(sfrhabitable, townhomehabitable, condohabitable, apthabitable, sfrnonhabitable, townhomenonhabitable, condononhabitable, aptnonhabitable) {
                        var schoolFee = ((sfrhabitable + townhomehabitable + condohabitable + apthabitable) * 2.97) + ((sfrnonhabitable + townhomenonhabitable + condononhabitable + aptnonhabitable) * 0.47);
                        return schoolFee;
                    }

                    //calc parked dedication fees (project based, removed for next function building based
                    //  function calcParkDedicationFee(sfrunits, sfrstandalone, sfrgranny, townhomeunits, condounits, aptunits) {
                    //     var sfrDetached = sfrstandalone * 11953;
                    //derived duplexes then add townhomes and condos
                    //   var sfrAttached = (((sfrunits - (sfrstandalone + sfrgranny)) + townhomeunits + condounits) * 11395);
                    //    var multifamily = (aptunits + sfrgranny) * 9653;
                    //   return (sfrDetached + sfrAttached + multifamily);
                    // }

                    //new park dedication fees on building based estimator
                    function calcParkDedicationFee(sfrduplex, sfrstandalone, sfrgranny, townhomeunits, condounits, aptunits) {
                        var sfrDetached = sfrstandalone * 11953;
                        //derived duplexes then add townhomes and condos
                        var sfrAttached = (sfrduplex + townhomeunits + condounits) * 11395;
                        var multifamily = (aptunits + sfrgranny) * 9653;
                        return (sfrDetached + sfrAttached + multifamily);
                    }

                    //inclusionary housing fee for projects with >= 20 units. Since our estimator is building based, need to ask user if their total project is >= 20 units
                    //$4.00 per sq habitable ft for sfr Detached
                    //$3.24 per sq habitable ft for sfr Attached(duplex), townhomes, condos, apartments
                    function calcInclusionaryHousingFee(sfrduplex, sfrstandalone, sfrgranny, townhomeunits, condounits, aptunits, yesorno, sfrHabitable, townhomeHabitable, condoHabitable, aptHabitable) {
                        var detachedOrAttached;
                        var totalInclusionaryHousingFee;

                        //if duplexes, use Attached for fee calculations, otherwise Detached fee
                        if (sfrduplex > 0) { detachedOrAttached = "Attached" } else { detachedOrAttached = "Detached" }

                        if (yesorno === "No") {
                            totalInclusionaryHousingFee = 0;
                        } else {
                            // use same rate for all since these are duplexes
                            if (detachedOrAttached === "Detached") {
                                totalInclusionaryHousingFee = ((sfrHabitable + townhomeHabitable + condoHabitable + aptHabitable) * 3.24);
                            }
                            // use fee for detached sfr along with condos/townhomes/apts 
                            else {
                                totalInclusionaryHousingFee = (sfrHabitable * 4.0) + ((townhomeHabitable + condoHabitable + aptHabitable) * 3.24);
                            }

                        }
                        return totalInclusionaryHousingFee;
                    }

                    //BUILDING CONSTRUCTION & IMPROVEMENT TAX
                    function calcConstructionTax(units) {

                        var singleFamilyLargeUnitFee = 750 * units[0];
                        var singleFamilySmallUnitFee = 600 * units[1];
                        var multiFamilyLargeUnitFee = 450 * units[2];
                        var multiFamilySmallUnitFee = 300 * units[3];

                        return singleFamilyLargeUnitFee +
                                singleFamilySmallUnitFee +
                                multiFamilyLargeUnitFee +
                                multiFamilySmallUnitFee
                    }

                    function calcSupplementalConstructionTax(units) {
                        return units * 1200;
                    }

                    function totalBuildingPermitFee(insFee) {
                        //????? maybe check divs for values instead of recalculating every time. maybe faster??

                        var totalBldg = (parseFloat(calcBldgInspectionFee(insFee)) + parseFloat(calcFireInspectionFee()) + parseFloat(calcHazmatInspectionFee()) + parseFloat(calcBuildingPlanCheckFee) + parseFloat(calcFireReviewFee(insFee)) + parseFloat(calcPlanningReviewFee(insFee)) + parseFloat(calcSolidWasteReviewFee()) + parseFloat(calcTechnologyFee(insFee)) + parseFloat(calcCommunityPlanningFee(insFee)) + parseFloat(calcPermitIssuanceFee()));
                        return totalBldg;
                    }

                    function getDisplayStyle() {
                        var e = document.getElementsByClassName('expand-fees');
                        if (e[0].style.display == 'block') {
                            var correctStyle = "none";
                        }
                        else {
                            var correctStyle = "block";
                        }
                        return correctStyle;
                    }

                    function expandOrContract(correctStyle) {
                        //loop through all the selections of divs with class of 'expand-fees'
                        if (correctStyle === "block") {
                            var correctedText = "Click to close detailed fee breakdown";
                        }
                        else {
                            var correctedText = "Click to open detailed fee breakdown";
                        }
                        return correctedText;
                    }

                    //returns what the controller needs
                    return {
                        getOutput: getOutput,
                        drawOutput: drawOutput,

                        figureResidentialWaterMeter: figureResidentialWaterMeter,
                        calcHomeRepairWaterFees: calcHomeRepairWaterFees,
                        calcNewDevelopmentWaterFees: calcNewDevelopmentWaterFees,
                        calcSewerConnectionFee: calcSewerConnectionFee,
                        getValuation: getValuation,

                        //INSPECTION FEES
                        calcInsFee: calcInsFee,
                        calcBldgInspectionFee: calcBldgInspectionFee,
                        calcFireInspectionFee: calcFireInspectionFee,
                        calcHazmatInspectionFee: calcHazmatInspectionFee,

                        //PLAN REVIEW FEES
                        calcBuildingPlanCheckFee: calcBuildingPlanCheckFee,
                        calcFireReviewFee: calcFireReviewFee,
                        calcPlanningReviewFee: calcPlanningReviewFee,
                        calcSolidWasteReviewFee: calcSolidWasteReviewFee,

                        //ADMINISTRATIVE FEES
                        calcTechnologyFee: calcTechnologyFee,
                        calcCommunityPlanningFee: calcCommunityPlanningFee,
                        calcPermitIssuanceFee: calcPermitIssuanceFee,
                        calcSMIPFee: calcSMIPFee,
                        calcBuildingStandardsFee: calcBuildingStandardsFee,

                        //ADDITIONAL TAXES & FEES
                        calcHomeRepairSchoolDistrictFee: calcHomeRepairSchoolDistrictFee,
                        calcNewDevelopmentResidentialSchoolDistrictFee: calcNewDevelopmentResidentialSchoolDistrictFee,
                        calcParkDedicationFee: calcParkDedicationFee,
                        calcInclusionaryHousingFee: calcInclusionaryHousingFee,
                        calcConstructionTax: calcConstructionTax,
                        calcSupplementalConstructionTax: calcSupplementalConstructionTax,

                        totalBuildingPermitFee: totalBuildingPermitFee,
                        getDisplayStyle: getDisplayStyle,
                        expandOrContract: expandOrContract
                    }


                };                                                                      //end model



