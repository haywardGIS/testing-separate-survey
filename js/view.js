///////Used by the controller to: set and get data; set an eventhandler/////////
var ViewHelper = function () {


    function getOldEstimate() {
        return $('#old-estimate-id').val();
    }
    function getTitle() {
        return $('#title').text();
    }
    function setTitle(value) {
        $('#title').text(value);
    }
    function getProjectName() {
        return $('#project-name').val();
    }
    function setProjectName(value) {
        $('#project-name').val(value);
    }
    function getToken() {
        return $('#token').val();
    }
    function setToken(value) {
        $('#token').val(value);
    }
    function getExistingHabitableSquareFootage() {
        return $('#existing-habitable-squarefootage').val();
    }
    function setExistingHabitableSquareFootage(value) {
        $('#existing-habitable-squarefootage').val(value);
    }
    function getExistingNonHabitableSquareFootage() {
        return $('#existing-nonhabitable-squarefootage').val();
    }
    function setExistingNonHabitableSquareFootage(value) {
        $('#existing-nonhabitable-squarefootage').val(value);
    }
    function getNewHabitableSquareFootage() {
        return $('#new-habitable-squarefootage').val();
    }
    function setNewHabitableSquareFootage(value) {
        $('#new-habitable-squarefootage').val(value);
    }
    function getNewNonHabitableSquareFootage() {
        return $('#new-nonhabitable-squarefootage').val();
    }
    function setNewNonHabitableSquareFootage(value) {
        $('#new-nonhabitable-squarefootage').val(value);
    }
    function getValuation() {
        return $('#valuation').val();
    }
    function setValuation(value) {
        console.log("in setValuation = " + value);
        $('#valuation').val(value);
    }
    function getNewWaterFixturesYN() {
        return $('#new-water-yes-or-no').val();
    }
    function setNewWaterFixturesYN(value) {
        $('#new-water-yes-or-no').val(value);
    }
    function getCurrentWaterMeter() {
        return $('#current-water-meter').val();
    }
    function setCurrentWaterMeter(value) {
        $('#current-water-meter').val(value);
    }
    function getNewWaterMeter() {
        return $('#new-water-meter').val();
    }
    function setNewWaterMeter(value) {
        $('#new-water-meter').val(value);
    }
    function getShowerTubCombo() {
        return $('#stcombo').val();
    }
    function setShowerTubCombo(value) {
        $('#stcombo').val(value);
    }
    function getTubOnly() {
        return $('#tub-only').val();
    }
    function setTubOnly(value) {
        $('#tub-only').val(value);
    }
    function getShowerOnly() {
        return $('#shower-only').val();
    }
    function setShowerOnly(value) {
        $('#shower-only').val(value);
    }
    function getJacuzzi() {
        return $('#jacuzzi').val();
    }
    function setJacuzzi(value) {
        $('#jacuzzi').val(value);
    }
    function getBathSink() {
        return $('#bath-sink').val();
    }
    function setBathSink(value) {
        $('#bath-sink').val(value);
    }
    function getToilet() {
        return $('#toilet').val();
    }
    function setToilet(value) {
        $('#toilet').val(value);
    }
    function getKitchenSink() {
        return $('#kitchen-sink').val();
    }
    function setKitchenSink(value) {
        $('#kitchen-sink').val(value);
    }
    function getDishwasher() {
        return $('#dishwasher').val();
    }
    function setDishwasher(value) {
        $('#dishwasher').val(value);
    }
    function getLaundrySink() {
        return $('#laundry-sink').val();
    }
    function setLaundrySink(value) {
        $('#laundry-sink').val(value);
    }
    function getClothesWasher() {
        return $('#clothes-washer').val();
    }
    function setClothesWasher(value) {
        $('#clothes-washer').val(value);
    }
    function getGarageSink() {
        return $('#garage-sink').val();
    }
    function setGarageSink(value) {
        $('#garage-sink').val(value);
    }
    function getBarSink() {
        return $('#bar-sink').val();
    }
    function setBarSink(value) {
        $('#bar-sink').val(value);
    }
    function getHoseBib() {
        return $('#hose-bib').val();
    }
    function setHoseBib(value) {
        $('#hose-bib').val(value);
    }

    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //BEGIN New Development Unit Divs (SFR, townhome, condo, apartment\\\\
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //New Development Residential Apartment units
    function getAptUnits() {
        return $('#apt-units').val();
    }
    function setAptUnits(value) {
        $('#apt-units').val(value);
    }
    //removed for building centered estimator, number of units = units in small bldg, no need to separate
    //  function getAptSmallBldgs() {
    //    return $('#apt-smallbuildings').val();
    //}
    //function setAptSmallBldgs(value) {
    //  $('#apt-smallbuildings').val(value);
    //}
    function getAptHabitable() {
        return $('#apt-habitablesqft').val();
    }
    function setAptHabitable(value) {
        $('#apt-habitablesqft').val(value);
    }
    function getAptNonHabitable() {
        return $('#apt-nonhabitablesqft').val();
    }
    function setAptNonHabitable(value) {
        $('#apt-nonhabitablesqft').val(value);
    }
    function getApt800() {
        return $('#apt-800sqft').val();
    }
    function setApt800(value) {
        $('#apt-800sqft').val(value);
    }

    //New Development Residential Condo units
    function getCondoUnits() {
        return $('#condo-units').val();
    }
    function setCondoUnits(value) {
        $('#condo-units').val(value);
    }
    //removed for building centered estimator, number of units = units in small bldg, no need to separate
    //    function getCondoSmallBldgs() {
    //      return $('#condo-smallbuildings').val();
    //}
    // function setCondoSmallBldgs(value) {
    //   $('#condo-smallbuildings').val(value);
    //}
    function getCondoHabitable() {
        return $('#condo-habitablesqft').val();
    }
    function setCondoHabitable(value) {
        $('#condo-habitablesqft').val(value);
    }
    function getCondoNonHabitable() {
        return $('#condo-nonhabitablesqft').val();
    }
    function setCondoNonHabitable(value) {
        $('#condo-nonhabitablesqft').val(value);
    }
    function getCondo800() {
        return $('#condo-800sqft').val();
    }
    function setCondo800(value) {
        $('#condo-800sqft').val(value);
    }

    //New Development Residential Townhome units
    function getTownhomeUnits() {
        return $('#townhome-units').val();
    }
    function setTownhomeUnits(value) {
        $('#townhome-units').val(value);
    }
    function getTownhomeType() {
        return $('#townhome-type').val();
    }
    function setTownhomeType(value) {
        $('#townhome-type').val(value);
    }
    function getTownhomeHabitable() {
        return $('#townhome-habitablesqft').val();
    }
    function setTownhomeHabitable(value) {
        $('#townhome-habitablesqft').val(value);
    }
    function getTownhomeNonHabitable() {
        return $('#townhome-nonhabitablesqft').val();
    }
    function setTownhomeNonHabitable(value) {
        $('#townhome-nonhabitablesqft').val(value);
    }
    function getTownhomeSize() {
        return $('#townhome-size').val();
    }
    function setTownhomeSize(value) {
        $('#townhome-size').val(value);
    }

    //New Development Single Family Residence (SFR) units
    function getPlannedDevelopment() {
        return $('#planned-development').val();
    }
    function setPlannedDevelopment(value) {
        $('#planned-development').val(value);
    }
    function getSFRStandalone() {
        return $('#sfr-standalone').val();
    }
    function setSFRStandalone(value) {
        $('#sfr-standalone').val(value);
    }
    function getSFRDuplexes() {
        return $('#sfr-duplexes').val();
    }
    function setSFRDuplexes(value) {
        $('#sfr-duplexes').val(value);
    }
    function getSFRInLaws() {
        return $('#sfr-inlaw').val();
    }
    function setSFRInLaws(value) {
        $('#sfr-inlaw').val(value);
    }
    function getSFRHabitable() {
        return $('#sfr-habitablesqft').val();
    }
    function setSFRHabitable(value) {
        $('#sfr-habitablesqft').val(value);
    }
    function getSFRNonHabitable() {
        return $('#sfr-nonhabitablesqft').val();
    }
    function setSFRNonHabitable(value) {
        $('#sfr-nonhabitablesqft').val(value);
    }
    function getSFR1500() {
        return $('#sfr-1500sqft').val();
    }
    function setSFR1500(value) {
        $('#sfr-1500sqft').val(value);
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //END New Development Unit Divs (SFR, townhome, condo, apartment\\\\
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //BEGIN New Development Residential Water Configurations\\\\\\\\\\\\
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    //array to help us dynamically update all water values, these are the endings of the div that we append to "wc" + number to get each individual div no matter how many configurations
    var arrayOfWaterDivEndings = ["-units", "-stcombo", "-tub-only", "-jacuzzi", "-shower-only", "-bath-sink", "-toilets", "-kitchen-sink", "-dishwasher", "-laundry-sink", "-clothes-washer",
        "-garage-sink", "-bar-sink", "-hose-bib", "-watermeter"];

    //?? need to figure out to get the water values to recalculate; maybe just make a new array? and call just passes in which water config, bascially replicating 'results' from the model
    function getWaterConfigValues(div) {
        return $('#' + div).val();
    }
    function setWaterConfigValues(value, number) {
        console.log("arrayofWaterDivEndings = " + arrayOfWaterDivEndings);
        console.log("arrayofWaterDivEndings[0] = " + arrayOfWaterDivEndings[0]);
        console.log("in setWaterConfigValus, value array = " + value);
        console.log("value length in setWaterConfigValues = " + value.length);
        for (j = 0; j < 15; j++) {
            var thisWaterConfigDiv = "wc" + number + arrayOfWaterDivEndings[j];
            $('#' + thisWaterConfigDiv).val(value.split(",")[j]);
        }
    }
    function setNewDevWaterMeter(div, value) {
        $('#' + div).val(value);
    }

    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //END New Development Residential Water Configurations\\\\\\\\\\\\\\
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    function getBldgInspFee() {
        return $('#est-bldg-inspection-fee').text();
    }
    function setBldgInspFee(value) {
        $('#est-bldg-inspection-fee').text(value);
    }
    function getBldgPlanCheckFee() {
        return $('#est-bldg-plan-check-fee').text();
    }
    function setBldgPlanCheckFee(value) {
        $('#est-bldg-plan-check-fee').text(value);
    }
    function getFireInspFee() {
        return $('#est-fire-inspection-fee').text();
    }
    function setFireInspFee(value) {
        $('#est-fire-inspection-fee').text(value);
    }
    function getHazInspFee() {
        return $('#est-hazmat-inspection-fee').text();
    }
    function setHazInspFee(value) {
        $('#est-hazmat-inspection-fee').text(value);
    }
    function getFireReviewFee() {
        return $('#est-fire-review-fee').text();
    }
    function setFireReviewFee(value) {
        $('#est-fire-review-fee').text(value);
    }
    function getPlanRevFee() {
        return $('#est-planning-review-fee').text();
    }
    function setPlanRevFee(value) {
        $('#est-planning-review-fee').text(value);
    }
    function getSolidWRevFee() {
        return $('#est-solid-waste-review-fee').text();
    }
    function setSolidWRevFee(value) {
        $('#est-solid-waste-review-fee').text(value);
    }
    function getTechFee() {
        return $('#est-technology-fee').text();
    }
    function setTechFee(value) {
        $('#est-technology-fee').text(value);
    }
    function getCommPlanFee() {
        return $('#est-community-planning-fee').text();
    }
    function setCommPlanFee(value) {
        $('#est-community-planning-fee').text(value);
    }
    function getPermIssFee() {
        return $('#est-permit-issuance-fee').text();
    }
    function setPermIssFee(value) {
        $('#est-permit-issuance-fee').text(value);
    }
    function getSMIPFee() {
        return $('#est-smip-fee').text();
    }
    function setSMIPFee(value) {
        console.log(value);
        $('#est-smip-fee').text(value);
    }
    function getBldgStandardsFee() {
        return $('#est-building-standards-fee').text();
    }
    function setBldgStandardsFee(value) {
        console.log(value);
        $('#est-building-standards-fee').text(value);
    }
    function getWaterMeterFee() {
        return $('#est-water-meter-fee').text();
    }
    function setWaterMeterFee(value) {
        $('#est-water-meter-fee').text(value);
    }
    function getRemoteFee() {
        return $('#est-remote-reading-fee').text();
    }
    function setRemoteFee(value) {
        $('#est-remote-reading-fee').text(value);
    }
    function getSewerConnectionFee() {
        return $('#est-sewer-connectino-fee').text();
    }
    function setSewerConnectionFee(value) {
        $('#est-sewer-connectino-fee').text(value);
    }
    function getSchoolDistrictFee() {
        return $('#est-school-district-fee').text();
    }
    function setSchoolDistrictFee(value) {
        $('#est-school-district-fee').text(value);
        console.log(getSchoolDistrictFee());
    }
    function getParkDedicationFee() {
        return $('#est-park-dedication-fee').text();
    }
    function setParkDedicationFee(value) {
        $('#est-park-dedication-fee').text(value);
    }
    function getInclusionaryHousingFee() {
        return $('#est-inclusionary-housing-fee').text();
    }
    function setInclusionaryHousingFee(value) {
        $('#est-inclusionary-housing-fee').text(value);
    }
    function getConstructionTax() {
        return $('#est-construction-improvement-tax').text();
    }
    function setConstructionTax(value) {
        $('#est-construction-improvement-tax').text(value);
    }
    function getSupplementalConstructionTax() {
        return $('#est-supplemental-construction-improvement-tax').text();
    }
    function setSupplementalConstructionTax(value) {
        $('#est-supplemental-construction-improvement-tax').text(value);
    }
    function getTotalFees() {
        $('#est-total-fee').text();
    }
    function setTotalFees(value) {
        $('#est-total-fee').text(value);
    }
    function getFeesPerUnit() {
        $('#est-fees-per-unit').text();
    }
    function setFeesPerUnit(value) {
        $('#est-fees-per-unit').text(value);
    }
    function setExpandOrContract(value) {
        $(".expand-fees").css("display", value);
    }
    function setExpandOrContractText(value) {
        $('#click-here').text(value);
    }

    function setActionListener(eventHandler) {
        //add on change event handler to all number fields added to #mainForm
        $("#mainForm").on("change", ":input[type='number']", eventHandler);

        //original event handlers, doesn't work with dynamically created divs, switched to code above
        //   $("#squarefootage").on("change", eventHandler);
        //    $("#valuation").on("change", eventHandler);

    }

    // function to hide per Unit Fees for Home Repair or Commerical estimates
    function hidePerUnitFees() {
        $('.fees-per-unit').addClass('hide')
    }

    // function to show per Unit Fees for New Development if it was turned off for Home Repair previously
    function showPerUnitFees() {
        $('.fees-per-unit').removeClass('hide')
    }

    // function to expand detailed fee sections
    function setClickToExpandListener(eventHandler) {
        $("#click-here").on("click", eventHandler);
    }

    function setOnLoadListener(eventHandler) {
        $(window).on("load", eventHandler);
    }


    //returns what the controller needs to perform it's tasks
    return {
        getOldEstimate: getOldEstimate,
        getTitle: getTitle,
        setTitle: setTitle,
        getProjectName: getProjectName,
        setProjectName: setProjectName,
        getToken: getToken,
        setToken: setToken,
        getExistingHabitableSquareFootage: getExistingHabitableSquareFootage,
        setExistingHabitableSquareFootage: setExistingHabitableSquareFootage,
        getExistingNonHabitableSquareFootage: getExistingNonHabitableSquareFootage,
        setExistingNonHabitableSquareFootage: setExistingNonHabitableSquareFootage,
        getNewHabitableSquareFootage: getNewHabitableSquareFootage,
        setNewHabitableSquareFootage: setNewHabitableSquareFootage,
        getNewNonHabitableSquareFootage: getNewNonHabitableSquareFootage,
        setNewNonHabitableSquareFootage: setNewNonHabitableSquareFootage,
        getValuation: getValuation,
        setValuation: setValuation,
        getNewWaterFixturesYN: getNewWaterFixturesYN,
        setNewWaterFixturesYN: setNewWaterFixturesYN,
        getCurrentWaterMeter: getCurrentWaterMeter,
        setCurrentWaterMeter: setCurrentWaterMeter,
        getNewWaterMeter: getNewWaterMeter,
        setNewWaterMeter: setNewWaterMeter,
        getShowerTubCombo: getShowerTubCombo,
        setShowerTubCombo: setShowerTubCombo,
        getTubOnly: getTubOnly,
        setTubOnly: setTubOnly,
        getShowerOnly: getShowerOnly,
        setShowerOnly: setShowerOnly,
        getJacuzzi: getJacuzzi,
        setJacuzzi: setJacuzzi,
        getBathSink: getBathSink,
        setBathSink: setBathSink,
        getToilet: getToilet,
        setToilet: setToilet,
        getKitchenSink: getKitchenSink,
        setKitchenSink: setKitchenSink,
        getDishwasher: getDishwasher,
        setDishwasher: setDishwasher,
        getLaundrySink: getLaundrySink,
        setLaundrySink: setLaundrySink,
        getClothesWasher: getClothesWasher,
        setClothesWasher: setClothesWasher,
        getGarageSink: getGarageSink,
        setGarageSink: setGarageSink,
        getBarSink: getBarSink,
        setBarSink: setBarSink,
        getHoseBib: getHoseBib,
        setHoseBib: setHoseBib,

        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        //BEGIN NEW DEVELOPMENT RESIDENTIAL FUNCTIONS\\\\
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        //APARTMENT DIVS
        getAptUnits: getAptUnits,
        setAptUnits: setAptUnits,
        // getAptSmallBldgs: getAptSmallBldgs,
        // setAptSmallBldgs: setAptSmallBldgs,
        getAptHabitable: getAptHabitable,
        setAptHabitable: setAptHabitable,
        getAptNonHabitable: getAptNonHabitable,
        setAptNonHabitable: setAptNonHabitable,
        getApt800: getApt800,
        setApt800: setApt800,

        //CONDO DIVS
        getCondoUnits: getCondoUnits,
        setCondoUnits: setCondoUnits,
        //getCondoSmallBldgs: getCondoSmallBldgs,
        //setCondoSmallBldgs: setCondoSmallBldgs,
        getCondoHabitable: getCondoHabitable,
        setCondoHabitable: setCondoHabitable,
        getCondoNonHabitable: getCondoNonHabitable,
        setCondoNonHabitable: setCondoNonHabitable,
        getCondo800: getCondo800,
        setCondo800: setCondo800,

        //TOWNHOME DIVS
        getTownhomeUnits: getTownhomeUnits,
        setTownhomeUnits: setTownhomeUnits,
        getTownhomeType: getTownhomeType,
        setTownhomeType: setTownhomeType,
        getTownhomeHabitable: getTownhomeHabitable,
        setTownhomeHabitable: setTownhomeHabitable,
        getTownhomeNonHabitable: getTownhomeNonHabitable,
        setTownhomeNonHabitable: setTownhomeNonHabitable,
        getTownhomeSize: getTownhomeSize,
        setTownhomeSize: setTownhomeSize,

        //SINGLE FAMILY RESIDENCE (SFR) DIVS
        getPlannedDevelopment: getPlannedDevelopment,
        setPlannedDevelopment: setPlannedDevelopment,
        getSFRStandalone: getSFRStandalone,
        setSFRStandalone: setSFRStandalone,
        getSFRDuplexes: getSFRDuplexes,
        setSFRDuplexes: setSFRDuplexes,
        getSFRInLaws: getSFRInLaws,
        setSFRInLaws: setSFRInLaws,
        getSFRHabitable: getSFRHabitable,
        setSFRHabitable: setSFRHabitable,
        getSFRNonHabitable: getSFRNonHabitable,
        setSFRNonHabitable: setSFRNonHabitable,
        getSFR1500: getSFR1500,
        setSFR1500: setSFR1500,

        //WATER CONFIGURATIONS FOR NEW DEVELOPMENT RESIDENTIAL
        getWaterConfigValues: getWaterConfigValues,
        setWaterConfigValues: setWaterConfigValues,
        setNewDevWaterMeter: setNewDevWaterMeter,

        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        //END NEW DEVELOPMENT RESIDENTIAL FUNCTIONS\\\\\\
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\         

        getBldgInspFee: getBldgInspFee,
        setBldgInspFee: setBldgInspFee,
        getBldgPlanCheckFee: getBldgPlanCheckFee,
        setBldgPlanCheckFee: setBldgPlanCheckFee,
        getFireInspFee: getFireInspFee,
        setFireInspFee: setFireInspFee,
        getHazInspFee: getHazInspFee,
        setHazInspFee: setHazInspFee,
        getFireReviewFee: getFireReviewFee,
        setFireReviewFee: setFireReviewFee,
        getPlanRevFee: getPlanRevFee,
        setPlanRevFee: setPlanRevFee,
        getSolidWRevFee: getSolidWRevFee,
        setSolidWRevFee: setSolidWRevFee,
        getTechFee: getTechFee,
        setTechFee: setTechFee,
        getCommPlanFee: getCommPlanFee,
        setCommPlanFee: setCommPlanFee,
        getPermIssFee: getPermIssFee,
        setPermIssFee: setPermIssFee,
        getSMIPFee: getSMIPFee,
        setSMIPFee: setSMIPFee,
        getBldgStandardsFee: getBldgStandardsFee,
        setBldgStandardsFee: setBldgStandardsFee,
        getWaterMeterFee: getWaterMeterFee,
        setWaterMeterFee: setWaterMeterFee,
        getRemoteFee: getRemoteFee,
        setRemoteFee: setRemoteFee,
        getSewerConnectionFee: getSewerConnectionFee,
        setSewerConnectionFee: setSewerConnectionFee,
        getSchoolDistrictFee: getSchoolDistrictFee,
        setSchoolDistrictFee: setSchoolDistrictFee,
        getParkDedicationFee: getParkDedicationFee,
        setParkDedicationFee: setParkDedicationFee,
        getInclusionaryHousingFee: getInclusionaryHousingFee,
        setInclusionaryHousingFee: setInclusionaryHousingFee,
        getConstructionTax: getConstructionTax,
        setConstructionTax: setConstructionTax,
        getSupplementalConstructionTax: getSupplementalConstructionTax,
        setSupplementalConstructionTax: setSupplementalConstructionTax,
        getTotalFees: getTotalFees,
        setTotalFees: setTotalFees,
        getFeesPerUnit: getFeesPerUnit,
        setFeesPerUnit: setFeesPerUnit,
        setExpandOrContract: setExpandOrContract,
        setExpandOrContractText: setExpandOrContractText,
        setOnLoadListener: setOnLoadListener,
        setActionListener: setActionListener,
        hidePerUnitFees: hidePerUnitFees,
        showPerUnitFees: showPerUnitFees,
        setClickToExpandListener: setClickToExpandListener
    }

};