<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//function randomKey($length) {
//    $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));

//    for($i=0; $i < $length; $i++) {
//        $key .= $pool[mt_rand(0, count($pool) - 1)];
//    }
//    return $key;
//}

//$randKey = randomKey(8);

//original typeform url copied to new survey when switching to building centric estimate
//$json_url = "https://api.typeform.com/v1/form/U0KNFv?key=5262ebfa73a30431442e857bcc4476cf375f188d";
//typeform url
$json_url = "https://api.typeform.com/v1/form/mcffiv?key=5262ebfa73a30431442e857bcc4476cf375f188d";
// echo "hola" . $_GET['token'];
if ($_GET['token']=="") {
	$json_url = $json_url . "&completed=true&order_by[]=date_submit,desc&limit=1";
}else{

	$json_url = $json_url . "&token=". $_GET['token'];
}


$json = file_get_contents($json_url);
$json=str_replace('},]',"}]",$json);
$data_a = json_decode($json, true);
//echo $data_j = json_encode($data_a)
//echo $json

// $ret = array();
// $ret[] = array($data_a["responses"][0]["token"], ["answers"][0]["listimage_23623293_choice"])
// echo $ret

//$answers[] = [{data_a["responses"][0]["token"], data_a["answers"][0]["listimage_23623293_choice"]}];
// $answers = array($data_a["responses"][0]["token"]
	// , $data_a["responses"][0]["answers"]["listimage_23623293_choice"]
	// , $data_a["responses"][0]["answers"]["list_23623907_choice"]
	// , $data_a["responses"][0]["answers"]["dropdown_23624252"]
	// , $data_a["responses"][0]["answers"]["list_23625394_choice"]
	// , $data_a["responses"][0]["answers"]["number_23625423"]);
//echo json_encode($answers);

//echo $answers

$anskeyval = array();
//$waterconfigval = array
//  (
//  array(50,1,2,1,2,1,2,1,2,1,2,1,2,1),
//  array(50,2,1,2,1,2,1,2,1,2,1,2,1,2)
//  );

$anskeyval['xtoken'] = $data_a["responses"][0]["token"];
//project name
$anskeyval['xprojname'] = $data_a["responses"][0]["answers"]["textfield_39426468"];
//project type
$anskeyval['xprojtype'] = $data_a["responses"][0]["answers"]["listimage_39426469_choice"];

//NEW DEVELOPMENT RESIDENTIAL
//Is the new deveelopment residential or commercial
if (isset($data_a["responses"][0]["answers"]["listimage_39426470_choice"])) {$anskeyval['newdev-residentialorcommercial'] = $data_a["responses"][0]["answers"]["listimage_39426470_choice"]; }
//condos, apartments, townhomes or sfr
if (isset($data_a["responses"][0]["answers"]["list_39426563_choice"])) {$anskeyval['newdev-unittype'] = $data_a["responses"][0]["answers"]["list_39426563_choice"]; }
//planned development??
if (isset($data_a["responses"][0]["answers"]["yesno_39426477"])) {$anskeyval['newres-pd'] = $data_a["responses"][0]["answers"]["yesno_39426477"]; }
// what type of single family residence?
if (isset($data_a["responses"][0]["answers"]["list_39426564_choice"])) {$anskeyval['newdev-sfrtype'] = $data_a["responses"][0]["answers"]["list_39426564_choice"]; }
// sfr habitable sq ft
if (isset($data_a["responses"][0]["answers"]["number_39426499"])) {$anskeyval['newres-sfrhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426499"]; }
//new sfr Non-Habitable square feet
if (isset($data_a["responses"][0]["answers"]["number_39426500"])) {$anskeyval['newres-sfrnonhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426500"]; }
// How many of the single family units have habitable square feet over 1500?
if (isset($data_a["responses"][0]["answers"]["number_39426501"])) {$anskeyval['newres-sfr1500'] = $data_a["responses"][0]["answers"]["number_39426501"]; }
//How many Townhome units are in this building?
if (isset($data_a["responses"][0]["answers"]["number_39426502"])) {$anskeyval['newres-townhomeunits'] = $data_a["responses"][0]["answers"]["number_39426502"]; }
//What is the total sum of habitable sq ft for all {{answer_39426502}} townhome units together?
if (isset($data_a["responses"][0]["answers"]["number_39426503"])) {$anskeyval['newres-townhomehabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426503"]; }
//What is the total sum of non-habitable sq ft for all <strong>{{answer_39426502}}<\/strong> townhome units together?
if (isset($data_a["responses"][0]["answers"]["number_39426504"])) {$anskeyval['newres-townhomenonhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426504"]; }
//Are these townhomes each built on an individual lot or does a larger lot contain a group of town homes?
if (isset($data_a["responses"][0]["answers"]["list_39426565_choice"])) {$anskeyval['newres-townhometype'] = $data_a["responses"][0]["answers"]["list_39426565_choice"]; }
//Of the {{answer_39426502}} townhome units, how many have habitable square foot floor plans greater than 1500 sq ft?
if (isset($data_a["responses"][0]["answers"]["number_39426505"])) {$anskeyval['newres-sfrtownhome1500'] = $data_a["responses"][0]["answers"]["number_39426505"]; }
//Of the {{answer_39426502}} townhome units, how many have habitable square foot floor plans greater than 800 sq ft?
if (isset($data_a["responses"][0]["answers"]["number_39426506"])) {$anskeyval['newres-multitownhome800'] = $data_a["responses"][0]["answers"]["number_39426506"]; }
//How many Condo units are in this building?
if (isset($data_a["responses"][0]["answers"]["number_39426507"])) {$anskeyval['newres-condounits'] = $data_a["responses"][0]["answers"]["number_39426507"]; }
//What is the total sum of habitable square feet for all {{answer_39426507}} condo units together?
if (isset($data_a["responses"][0]["answers"]["number_39426508"])) {$anskeyval['newres-condohabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426508"]; }
//What is the total sum of non-habitable square feet for all <strong>{{answer_39426507}}<\/strong> condo units together?
if (isset($data_a["responses"][0]["answers"]["number_39426509"])) {$anskeyval['newres-condononhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426509"]; }
//Of the {{answer_39426507}} condo units, how many have habitable square foot floor plans greater than 800 sq ft?
if (isset($data_a["responses"][0]["answers"]["number_39426510"])) {$anskeyval['newres-condo800'] = $data_a["responses"][0]["answers"]["number_39426510"]; }
//How many Apartment units are in this building?
if (isset($data_a["responses"][0]["answers"]["number_39426511"])) {$anskeyval['newres-aptunits'] = $data_a["responses"][0]["answers"]["number_39426511"]; }
//What is the total sum of habitable square feet for all {{answer_39426511}} apartment units together?
if (isset($data_a["responses"][0]["answers"]["number_39426512"])) {$anskeyval['newres-apthabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426512"]; }
//What is the total sum of non-habitable square feet for all <strong>{{answer_39426511}}<\/strong> apartment units together?
if (isset($data_a["responses"][0]["answers"]["number_39426513"])) {$anskeyval['newres-aptnonhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426513"]; }
//Of the {{answer_39426511}} apartment units, how many have habitable square foot floor plans greater than 800 sq ft?
if (isset($data_a["responses"][0]["answers"]["number_39426514"])) {$anskeyval['newres-apt800'] = $data_a["responses"][0]["answers"]["number_39426514"]; }
//Will the entire project (all buildings combined) contain more than 20 units.
if (isset($data_a["responses"][0]["answers"]["yesno_39426476"])) {$anskeyval['newres-housinginclusionary'] = $data_a["responses"][0]["answers"]["yesno_39426476"]; }
//Do you have a valuation (cost estimate) for this project?
if (isset($data_a["responses"][0]["answers"]["yesno_39426475"])) {$anskeyval['newres-knowvaluation'] = $data_a["responses"][0]["answers"]["yesno_39426475"]; }
//Enter the valuation.
if (isset($data_a["responses"][0]["answers"]["number_39426515"])) {$anskeyval['newres-valuation'] = $data_a["responses"][0]["answers"]["number_39426515"]; }

//How many different types of water configurations will there be in this project?
if (isset($data_a["responses"][0]["answers"]["number_39426516"])) {$anskeyval['newres-waterconfignumber'] = $data_a["responses"][0]["answers"]["number_39426516"]; }

$ary_waterConfig = array();
//if there are units in water config 1 (of course there are), fill in variables
if (isset($data_a["responses"][0]["answers"]["number_39426517"])) {
    $ary_waterConfig1 = array();
    //Unit Type 1<br \/><\/strong>How many units have this water fixture configuration?
    if (isset($data_a["responses"][0]["answers"]["number_39426517"])) {$ary_waterConfig1[0] = $data_a["responses"][0]["answers"]["number_39426517"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426518"])) {$ary_waterConfig1[1] = $data_a["responses"][0]["answers"]["number_39426518"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426519"])) {$ary_waterConfig1[2] = $data_a["responses"][0]["answers"]["number_39426519"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426520"])) {$ary_waterConfig1[3] = $data_a["responses"][0]["answers"]["number_39426520"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426521"])) {$ary_waterConfig1[4] = $data_a["responses"][0]["answers"]["number_39426521"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426522"])) {$ary_waterConfig1[5] = $data_a["responses"][0]["answers"]["number_39426522"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426523"])) {$ary_waterConfig1[6] = $data_a["responses"][0]["answers"]["number_39426523"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426524"])) {$ary_waterConfig1[7] = $data_a["responses"][0]["answers"]["number_39426524"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426525"])) {$ary_waterConfig1[8] = $data_a["responses"][0]["answers"]["number_39426525"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426526"])) {$ary_waterConfig1[9] = $data_a["responses"][0]["answers"]["number_39426526"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426527"])) {$ary_waterConfig1[10] = $data_a["responses"][0]["answers"]["number_39426527"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426528"])) {$ary_waterConfig1[11] = $data_a["responses"][0]["answers"]["number_39426528"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426529"])) {$ary_waterConfig1[12] = $data_a["responses"][0]["answers"]["number_39426529"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426530"])) {$ary_waterConfig1[13] = $data_a["responses"][0]["answers"]["number_39426530"]; }

    $ary_waterConfig[0] = $ary_waterConfig1;
}

//if there are units in water config 2, fill in variables
if (isset($data_a["responses"][0]["answers"]["number_39426531"])) {
    $ary_waterConfig2 = array();
        //Unit Type 2 How many units have this water fixture configuration?
    if (isset($data_a["responses"][0]["answers"]["number_39426531"])) {$ary_waterConfig2[0] = $data_a["responses"][0]["answers"]["number_39426531"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426532"])) {$ary_waterConfig2[1] = $data_a["responses"][0]["answers"]["number_39426532"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426533"])) {$ary_waterConfig2[2] = $data_a["responses"][0]["answers"]["number_39426533"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426534"])) {$ary_waterConfig2[3] = $data_a["responses"][0]["answers"]["number_39426534"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426535"])) {$ary_waterConfig2[4] = $data_a["responses"][0]["answers"]["number_39426535"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426536"])) {$ary_waterConfig2[5] = $data_a["responses"][0]["answers"]["number_39426536"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426537"])) {$ary_waterConfig2[6] = $data_a["responses"][0]["answers"]["number_39426537"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426538"])) {$ary_waterConfig2[7] = $data_a["responses"][0]["answers"]["number_39426538"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426539"])) {$ary_waterConfig2[8] = $data_a["responses"][0]["answers"]["number_39426539"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426540"])) {$ary_waterConfig2[9] = $data_a["responses"][0]["answers"]["number_39426540"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426541"])) {$ary_waterConfig2[10] = $data_a["responses"][0]["answers"]["number_39426541"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426542"])) {$ary_waterConfig2[11] = $data_a["responses"][0]["answers"]["number_39426542"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426543"])) {$ary_waterConfig2[12] = $data_a["responses"][0]["answers"]["number_39426543"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426544"])) {$ary_waterConfig2[13] = $data_a["responses"][0]["answers"]["number_39426544"]; }
    $ary_waterConfig[1] = $ary_waterConfig2;
}

//if there are units in water config 3, fill in variables
if (isset($data_a["responses"][0]["answers"]["number_39426545"])) {
    $ary_waterConfig3 = array();
    //Unit Type 3 How many units have this water fixture configuration?
    if (isset($data_a["responses"][0]["answers"]["number_39426545"])) {$ary_waterConfig3[0] = $data_a["responses"][0]["answers"]["number_39426545"]; } //units
    if (isset($data_a["responses"][0]["answers"]["number_39426546"])) {$ary_waterConfig3[1] = $data_a["responses"][0]["answers"]["number_39426546"]; } //stcombo
    if (isset($data_a["responses"][0]["answers"]["number_39426547"])) {$ary_waterConfig3[2] = $data_a["responses"][0]["answers"]["number_39426547"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426548"])) {$ary_waterConfig3[3] = $data_a["responses"][0]["answers"]["number_39426548"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426549"])) {$ary_waterConfig3[4] = $data_a["responses"][0]["answers"]["number_39426549"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426550"])) {$ary_waterConfig3[5] = $data_a["responses"][0]["answers"]["number_39426550"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426551"])) {$ary_waterConfig3[6] = $data_a["responses"][0]["answers"]["number_39426551"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426552"])) {$ary_waterConfig3[7] = $data_a["responses"][0]["answers"]["number_39426552"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426553"])) {$ary_waterConfig3[8] = $data_a["responses"][0]["answers"]["number_39426553"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426554"])) {$ary_waterConfig3[9] = $data_a["responses"][0]["answers"]["number_39426554"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426555"])) {$ary_waterConfig3[10] = $data_a["responses"][0]["answers"]["number_39426555"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426556"])) {$ary_waterConfig3[11] = $data_a["responses"][0]["answers"]["number_39426556"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426557"])) {$ary_waterConfig3[12] = $data_a["responses"][0]["answers"]["number_39426557"]; }
    if (isset($data_a["responses"][0]["answers"]["number_39426558"])) {$ary_waterConfig3[13] = $data_a["responses"][0]["answers"]["number_39426558"]; }
    $ary_waterConfig[2] = $ary_waterConfig3;
}

//$anskeyval['newres-waterconfigs'] = $waterconfigval;
$anskeyval['wc'] = $ary_waterConfig;

//HOME REMODELS
//Are you adding new square footage to your home?
if (isset($data_a["responses"][0]["answers"]["yesno_39426472"]))  { $anskeyval['hr-newsqftyesorno'] = $data_a["responses"][0]["answers"]["yesno_39426472"]; }
//How much new habitable square footage will be added?
if (isset($data_a["responses"][0]["answers"]["number_39426478"]))  { $anskeyval['hr-newhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426478"]; }
//How much new non-habitable square footage will be added?
if (isset($data_a["responses"][0]["answers"]["number_39426479"]))  { $anskeyval['hr-newnonhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426479"]; }
//How much existing habitable square footage will be worked on during this project?
if (isset($data_a["responses"][0]["answers"]["number_39426480"]))  { $anskeyval['hr-exhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426480"]; }
//How much existing non-habitable square footage will be worked on during this project?
if (isset($data_a["responses"][0]["answers"]["number_39426481"])) { $anskeyval['hr-exnonhabitablesqft'] = $data_a["responses"][0]["answers"]["number_39426481"]; }
//Do you know the valuation of your project?
if (isset($data_a["responses"][0]["answers"]["list_39426559_choice"]))  { $anskeyval['hr-knowvaluation'] = $data_a["responses"][0]["answers"]["list_39426559_choice"];}
//What is the Project Valuation (Cost)?
if (isset($data_a["responses"][0]["answers"]["number_39426482"]))  { $anskeyval['hr-valuation'] = $data_a["responses"][0]["answers"]["number_39426482"]; }
//Will additional water fixtures be added during this project?
if (isset($data_a["responses"][0]["answers"]["yesno_39426473"])) { $anskeyval['hr-newwateryesorno'] = $data_a["responses"][0]["answers"]["yesno_39426473"]; }
//What is the current water meter size?
if (isset($data_a["responses"][0]["answers"]["list_39426560_choice"]))  { $anskeyval['hr-currentwatermeter'] = $data_a["responses"][0]["answers"]["list_39426560_choice"];}
//Bathroom Fixtures.<\/strong><br \/>How many bathtub\/shower combos?
if (isset($data_a["responses"][0]["answers"]["number_39426483"]))  { $anskeyval['hr-stcombo'] = $data_a["responses"][0]["answers"]["number_39426483"]; }
//Bathroom Fixtures.<\/strong><br \/>How many bathtubs without a shower?
if (isset($data_a["responses"][0]["answers"]["number_39426484"]))  { $anskeyval['hr-tubonly'] = $data_a["responses"][0]["answers"]["number_39426484"]; }
//Bathroom Fixtures.<\/strong><br \/>How many jacuzzi bathtubs?
if (isset($data_a["responses"][0]["answers"]["number_39426485"]))  { $anskeyval['hr-jacuzzi'] = $data_a["responses"][0]["answers"]["number_39426485"]; }
//Bathroom Fixtures.<\/strong><br \/>How many shower stalls?
if (isset($data_a["responses"][0]["answers"]["number_39426486"]))  { $anskeyval['hr-shower'] = $data_a["responses"][0]["answers"]["number_39426486"]; }
//Bathroom Fixtures.<\/strong><br \/>How many bathroom sinks?
if (isset($data_a["responses"][0]["answers"]["number_39426487"]))  { $anskeyval['hr-bathsink'] = $data_a["responses"][0]["answers"]["number_39426487"]; }
//Bathroom Fixtures.<\/strong><br \/>How many toilets?
if (isset($data_a["responses"][0]["answers"]["number_39426488"]))  { $anskeyval['hr-toilet'] = $data_a["responses"][0]["answers"]["number_39426488"]; }
//Kitchen Fixtures.<\/strong><br \/>How many kitchen sinks?
if (isset($data_a["responses"][0]["answers"]["number_39426489"]))  { $anskeyval['hr-kitchensink'] = $data_a["responses"][0]["answers"]["number_39426489"]; }
//Kitchen Fixtures.<\/strong><br \/>How many dishwashers?
if (isset($data_a["responses"][0]["answers"]["number_39426490"]))  { $anskeyval['hr-dishwasher'] = $data_a["responses"][0]["answers"]["number_39426490"]; }
//Laundry Rooms<\/strong><br \/>How many sinks?
if (isset($data_a["responses"][0]["answers"]["number_39426491"]))  { $anskeyval['hr-laundrysink'] = $data_a["responses"][0]["answers"]["number_39426491"]; }
//Laundry Rooms<\/strong><br \/>How many clothes washers?
if (isset($data_a["responses"][0]["answers"]["number_39426492"]))  { $anskeyval['hr-clotheswasher'] = $data_a["responses"][0]["answers"]["number_39426492"]; }
//ther Fixtures<\/strong><br \/>How many garage sinks?
if (isset($data_a["responses"][0]["answers"]["number_39426493"]))  { $anskeyval['hr-garagesink'] = $data_a["responses"][0]["answers"]["number_39426493"]; }
//Other Fixtures<\/strong><br \/>How many bar sinks?
if (isset($data_a["responses"][0]["answers"]["number_39426494"]))  { $anskeyval['hr-barsink'] = $data_a["responses"][0]["answers"]["number_39426494"]; }
//Other Fixtures<\/strong><br \/>How many hose bibs?
if (isset($data_a["responses"][0]["answers"]["number_39426495"]))  { $anskeyval['hr-hosebib'] = $data_a["responses"][0]["answers"]["number_39426495"]; }


echo json_encode($anskeyval);

?>
